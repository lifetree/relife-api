var root = {
	"swagger": "2.0",
	"info": {
		"version": "0.0.1",
		"title": "LifeTree",
		"description": "**Драфт документации**\n\nДанный проект описывает публичные контракты API в проекте LifeTree.\n"
	},
	"basePath": "/api",
	"schemes": [
		"https"
	],
	"consumes": [
		"application/json",
		"application/x-www-form-urlencoded"
	],
	"produces": [
		"application/json",
		"image/*"
	],
	securityDefinitions: {
		basicAuth: {
			type: 'basic',
			description: 'Базовая авторизация, пароль + логи (HTTP Basic Auth)'
		}
	},

	/**
	 * Переменные которые будут проставлятся по умолчанию
	 */
	variables: ['userid'],

	paths: {
		'/session':  require('./resources/session'),
		'/users': require('./resources/users'),
		'/users/{userid}': require('./resources/user'),
		'/users/{userid}/photos': require('./resources/photos'),
		'/users/{userid}/photos/{photo}': require('./resources/photo'),
		'/users/{userid}/cores': require('./resources/cores'),
		'/users/{userid}/cores/{coreid}': require('./resources/core'),
		'/users/{userid}/milestones': require('./resources/milestones'),
		'/users/{userid}/milestones/{mid}': require('./resources/milestone'),
		'/users/{userid}/milestones/{mid}/tasks': require('./resources/tasks'),
		'/users/{userid}/milestones/{mid}/tasks/{taskid}': require('./resources/task'),

		// reports
		'/users/{userid}/tasks': require('./resources/reports/schedule')
	},

	definitions: {
		Action: require('./scheme/Action'),
		Task: require('./scheme/Task'),
		User: require('./scheme/User'),
		Milestone: require('./scheme/Milestone'),
		Habit: require('./scheme/Habit'),
		Goal: require('./scheme/Goal'),
		Core: require('./scheme/Core'),
		Session: require('./scheme/Session'),
		Photo: require('./scheme/Photo')
	}

};

exports.getSwaggerSpec = function (config) {
	var spec = JSON.parse(JSON.stringify(root)),
		key, method;

	spec.host = config.app.host;

	if (config.app.port) {
		spec.host +=  ':' + config.app.port;
	}

	for (key in spec.paths) {

		for (method in spec.paths[key]) {
			delete spec.paths[key][method].__controller;
			delete spec.paths[key][method].__validateSchema;
		}

	}

	for (key in spec.definitions) {
		delete spec.definitions[key].links;
	}

	return spec;
};

exports.getDescriptor = function () {

	return {

	};

};

exports.getSpec = function () {
	return root;
};
