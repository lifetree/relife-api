module.exports = {

	"get": {
		"tags": ["session"],
		"__controller": "session.get",
		"description": "Метод создает новую  или получает текущую сессию пользователя",
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"variables": {
			"id": "userid"
		},
		"parameters": [
		],
		"responses": {
			"200": {
				"schema": {
					"$ref": "#/definitions/Session"
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			}
		}

	}

};
