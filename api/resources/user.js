module.exports = {

	"get": {
		"tags": ["users"],
		"__controller": "user.get",
		"description": "Метод возвращает данные о пользователе системы",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"schema": {
					"$ref": "#/definitions/User"
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			},
			"404":{
				"description": "Такого пользователя нет"
			}
		}

	},

	"patch": {
		"tags": ["users"],
		"__controller": "user.patch",
		"description": "Метод обновляет данные о пользователе",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "body",
				"description": "Данные о пользователе",
				"required": true,
				"schema": {
					"$ref": "#/definitions/User"
				}
			}
		],
		"responses": {
			"200": {
				"description": "Пользователь успешно обновлен"
			},
			"400":{
				"description": "Не все поля заполненны верно"
			},
			"403":{
				"description": "Пользователь не может редактировать данный профиль"
			},
			"404":{
				"description": "Такого пользователя нет"
			},
			"409":{
				"description": "Попытка изменить фиксированые поля (id, createdDate)"
			}
		}

	}


};
