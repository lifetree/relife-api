module.exports = {

	"get": {
		"tags": ["tasks"],
		"__controller": "tasks.one",
		"description": "Метод получает данные о конкретном таске",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор milestone к которому привязывается задача",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "taskid",
				"description": "Идентификатор таска",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"$ref": "#/definitions/Task"
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	},

	"patch": {
		"tags": ["tasks"],
		"__controller": "tasks.patch",
		"description": "Метод обновляет данные о таске, если таск являеться частью Habit то обновлянеие поля failed в true приведет к генерация пеналти",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор milestone к которому привязывается задача",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "taskid",
				"description": "Идентификатор таска",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "body",
				"description": "Данные которые необходимо изменить в таске",
				"required": true,
				"schema": {
					"$ref": "#/definitions/Task"
				}
			}
		],
		"responses": {
			"200": {
				"description": "Таск успешно обновлен"
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	}

};
