module.exports = {

	"get": {
		"tags": ["cores"],
		"__controller": "cores.list",
		"__alias": "list",
		"description": "Метод получает ценности пользователя",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "id",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"schema": {
					"description": "Список пользовательских ценностей",
					type: 'array',
					items: {
						"$ref": "#/definitions/Core"
					}
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	},

	"post": {
		"tags": ["cores"],
		"__controller": "cores.create",
		"description": "Метод создает ценность пользователя",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "body",
				"name": "body",
				"description": "Данные о новой ценности пользователя",
				"required": true,
				"schema": {
					"$ref": "#/definitions/Core"
				}
			}
		],
		"responses": {
			"200": {
				"schema": {
					"description": "Данные созданной ценности пользователя",
					"$ref": "#/definitions/Core"
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			}
		}
	}

};
