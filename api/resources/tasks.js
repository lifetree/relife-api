module.exports = {

	"get": {
		"tags": ["tasks"],
		"__controller": "tasks.list",
		"__alias": "list",
		"description": "Метод получает получает спиоск тасков пользователя для данного milestone",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор milestone к которому привязывается задача",
				"required": true,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "skip",
				"description": "Сколько пропустить элементов",
				"required": false,
				"type": "boolean"
			},
			{
				"in": "query",
				"name": "limit",
				"description": "Сколько взять элементов в выборку",
				"required": false,
				"type": "boolean"
			}
		],
		"responses": {
			"200": {
				"type": "array",
				"schema": {
					"$ref": "#/definitions/Task"
				}
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"400": {
				"description": "Не все поля заполненны корректно"
			},
			"403": {
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	},

	"post": {
		"tags": ["tasks"],
		"__controller": "tasks.create",
		"description": "Метод создает новый таск",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор milestone к которому привязывается задача",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "body",
				"description": "Данные о новом таске",
				"required": true,
				"schema": {
					"type": "object",
					"$ref": "#/definitions/Task"
				}
			}
		],
		"responses": {
			"200": {
				"description": "Task успешно создан",
				"schema": {
					"$ref": "#/definitions/Task"
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	}

};
