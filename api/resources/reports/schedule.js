module.exports = {

	"get": {
		"tags": ["reports"],
		"__controller": "schedule.list",
		"__alias": "list",
		"description": "Метод генерирует отчет, с рассписанием задачь пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "mids",
				"description": "Идентификаторы milestones, для которых нужно вернуть таски",
				"required": false,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "skip",
				"description": "Сколько пропустить элементов",
				"required": false,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "limit",
				"description": "Сколько взять элементов в выборку (по умолчанию 30)",
				"required": false,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "start",
				"description": "Дата не меньше которой вернуть таски (если не передано, то берется текущая)",
				"required": false,
				"type": "timestamp"
			},
			{
				"in": "query",
				"name": "end",
				"description": "Дата не больше которой вернуть таски",
				"required": false,
				"type": "timestamp"
			},
			{
				"in": "query",
				"name": "failed",
				"description": "Брать только таски с зафейлиными actions",
				"required": false,
				"type": "boolean"
			},
			{
				"in": "query",
				"name": "completed",
				"description": "Брать только таски с завершенными actions",
				"required": false,
				"type": "boolean"
			}

		],
		"responses": {
			"200": {
				"type": "array",
				"schema": {
					"$ref": "#/definitions/Task"
				}
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	}
}
