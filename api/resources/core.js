module.exports = {

	"get": {
		"tags": ["cores"],
		"__controller": "cores.one",
		"description": "Метод получает конкретную ценность пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "coreid",
				"description": "Идентификатор ценности",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"schema": {
					"$ref": "#/definitions/Core"
				}
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может просматривать данную ценность"
			}

		}
	},

	"patch": {
		"tags": ["cores"],
		"__controller": "cores.patch",
		"description": "Метод обновляет ценность пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "coreid",
				"description": "Идентификатор ценности",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "body",
				"description": "Данные о пользователе",
				"required": true,
				"schema": {
					"$ref": "#/definitions/User"
				}
			}
		],
		"responses": {
			"200": {
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может редактировать данную ценность"
			}

		}
	},

	"delete": {
		"tags": ["cores"],
		"__controller": "cores.remove",
		"description": "Метод удаляет ценность пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "coreid",
				"description": "Идентификатор ценности",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может удалить данную ценность"
			}

		}
	}


};
