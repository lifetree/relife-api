module.exports = {

	"get": {
		"tags": ["milestones"],
		"__controller": "milestones.list",
		"__alias": "list",
		"description": "Метод получает майлстоуны пользователя," +
				" без указания параметров список будет содержать только не завершенные, не удаленные малстоуны",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "id",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "skip",
				"description": "Сколько пропустить элементов",
				"required": false,
				"type": "boolean"
			},
			{
				"in": "query",
				"name": "limit",
				"description": "Сколько взять элементов в выборку",
				"required": false,
				"type": "boolean"
			},
			{
				"in": "query",
				"name": "deleted",
				"description": "Показать только удаленные мейлстоуны",
				"required": false,
				"type": "boolean"
			},
			{
				"in": "query",
				"name": "completed",
				"description": "Показать только завершенные мейлстоуны",
				"required": false,
				"type": "boolean"
			},
			{
				"in": "query",
				"name": "parent",
				"description": "Идентификатор родительского мейлстоуна",
				"required": false,
				"type": "integer"
			},
			{
				"in": "query",
				"name": "cores",
				"description": "Идентификаторы ценностей перечисленные через запятую, для которых вернуть мейлстоуны (1,2,3)",
				"required": false,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"schema": {
					"description": "Список пользовательских майлстоунов",
					type: 'array',
					items: {
						"$ref": "#/definitions/Milestone"
					}
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	},

	"post": {
		"tags": ["milestones"],
		"__controller": "milestones.create",
		"description": "Метод создания Milestone",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "id",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "body",
				"description": "Данные о новом milestone",
				"required": true,
				"schema": {
					"type": "object",
					"$ref": "#/definitions/Milestone"
				}
			}
		],
		"responses": {
			"200": {
				"description": "Milestone успешно создан",
				"schema": {
					"$ref": "#/definitions/Milestone"
				}
			},
			"400":{
				"description": "Не все поля заполненны верно"
			},
			"403":{
				"description": "Пользователь не может редактировать данный профиль"
			}
		}
	}

};
