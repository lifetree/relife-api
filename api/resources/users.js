module.exports = {

	"post": {
		"tags": ["users", "signup"],
		"__controller": "users.create",
		"__validateSchema": "User",
		"description": "Метод создает нового пользователя системы",
		"parameters": [
			{
				"in": "body",
				"name": "body",
				"description": "Данные о новом пользователе",
				"required": true,
				"schema": {
					"type": "object",
					"properties": {
						"name": {
							"type": "string"
						},
						"email": {
							"type": "string",
							"format": "email"
						},
						"pwd": {
							"description": "Пароль нового пользователя",
							"type": "string"
						},
						"birthDate": {
							"description": "дата рождения пользователя",
							"type": "integer",
							"format": "timestamp"
						}
					}
				}
			}
		],
		"responses": {
			"200": {
				"schema": {
					"description": "Данные о вновь созданном пользователе",
					"$ref": "#/definitions/User"
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			}
		}
	}

};
