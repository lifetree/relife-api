module.exports = {

	"get": {
		"tags": ["photos"],
		"__controller": "photos.get",
		"description": "Метод возвращает фотографию пользователя",
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "photo",
				"description": "Идентификатор фотографии",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
			},
			"404":{
				"description": "Такого пользователя нет"
			}
		}
	},

	"delete": {
		"tags": ["photos"],
		"__controller": "photos.remove",
		"description": "Метод удаляет фотографию пользователя",
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "photo",
				"description": "Идентификатор фотографии",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
			},
			"404":{
				"description": "Такого пользователя нет"
			}
		}
	}

};
