module.exports = {

	"get": {
		"tags": ["milestones"],
		"__controller": "milestone.one",
		"description": "Метод получает майлстоун пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор майлстоуна",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"schema": {
					"$ref": "#/definitions/Milestone"
				}
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может просматривать данный майлстоун"
			}

		}
	},

	"patch": {
		"tags": ["milestones"],
		"__controller": "milestone.patch",
		"description": "Метод обновляет майлстоун пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор майлстоуна",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "body",
				"description": "Данные которые нужно обновить",
				"required": true,
				"schema": {
					"$ref": "#/definitions/Milestone"
				}
			}
		],
		"responses": {
			"200": {
				"schema": {
					"$ref": "#/definitions/Milestone"
				}
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может просматривать данный майлстоун"
			}

		}
	},

	"delete": {
		"tags": ["milestones"],
		"__controller": "milestone.remove",
		"description": "Метод удаляет milestone пользователя",
		"grants": {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "path",
				"name": "mid",
				"description": "Идентификатор milestone",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
			},
			"401": {
				"description": "Пользователь не авторизован"
			},
			"403": {
				"description": "Пользователь не может удалить данный milestone"
			}

		}
	}

};
