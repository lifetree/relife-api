module.exports = {

	"post": {
		"tags": ["photos", "upload", "external"],
		"__controller": "photos.upload",
		"description": "Метод загружает фотку",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "userid",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			},
			{
				"in": "body",
				"name": "photo",
				"description": "Фотографмия пользователя",
				"required": true,
				"type": "blob"
			}
		],
		"responses": {
			"200": {
				"schema": {
					"$ref": "#/definitions/Photo"
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			},
			"404":{
				"description": "Такого пользователя нет"
			}
		}

	},

	"get": {
		"tags": ["photos"],
		"__controller": "photos.list",
		"__alias": "list",
		"description": "Метод получает список фотографий пользоватлея",
		"grants":  {
			'owner': {
				in: 'uri',
				value: 'userid'
			}
		},
		"security": [
			{
				basicAuth: []
			},
			{
				token: []
			}
		],
		"parameters": [
			{
				"in": "path",
				"name": "id",
				"description": "Идентификатор пользователя",
				"required": true,
				"type": "integer"
			}
		],
		"responses": {
			"200": {
				"schema": {
					type: 'array',
					items: {
						"description": "Спиосок пользовательских фоточек",
						"type": "integer"
					}
				}
			},
			"401":{
				"description": "Пользователь не авторизован"
			},
			"400":{
				"description": "Не все поля заполненны корректно"
			},
			"403":{
				"description": "Пользователь не может просматривать данный профиль"
			}
		}
	}


};
