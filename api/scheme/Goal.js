module.exports =  {
	"type": "object",
	"description": "Пользовательская цель, имеет обязательного родителя - ценность или другая цель.",

	"properties": {
		"avatar": {
			"description": "Ссылка на загруженную аватарку",
			"type": "string",
			"format": "url"
		},
		"name": {
			"description": "Сформулированная цель",
			"type": "string"
		},
		"reason": {
			"description": "Информация, почему эта цель важна пользователю",
			"type": "string"
		},
		"criteria": {
			"description": "Критерий заверщения цели",
			"type": "string"
		},
		"plan": {
			"description": "Общий план по достижению цели",
			"type": "string"
		}
	},

	"required": ["name",  "reason", "criteria", "plan"]

};
