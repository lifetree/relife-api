module.exports =  {
	"type": "object",
	"description": "Сессия пользователя",

	"properties": {
		"token": {
			"description": "Токен сессии, может использоватся для авторизации",
			"type": "string"
		},
		"id": {
			"description": "Идентификатор пользователя",
			"type": "integer"
		}
	}
};
