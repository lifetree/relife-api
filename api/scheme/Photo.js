module.exports =  {
	"type": "object",
	"description": "Фотография пользователя",

	"properties": {
		"id": {
			"description": "Идентификатор фотографии",
			"type": "string"
		},
		"ownerId": {
			"description": "Идентификатор пользователя, которому принадлежит эта фотка",
			"type": "integer"
		}
	},

	"required": ["id", "ownerId"]
};
