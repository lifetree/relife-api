module.exports =  {
	"type": "object",
	"description": "Пользовательский таск, может быть связан или с привычкой или с целью.",

	"properties": {
		"ownerId": {
			"type": "integer"
		},
		"id": {
			"type": "integer"
		},
		"name": {
			"type": "string"
		},
		"milestoneId": {
			"description": "Родительская цель или привычка",
			"type": "integer"
		},
		"coreId": {
			"type": "integer"
		},
		"recurrent": {
			"type": "boolean",
			"description": "Признак того, что Таск рекуррентный"
		},
		"penalized": {
			"type": "boolean",
			"description": "Признак того, что на не выполнения action нужно начилять пеналти"
		},
		"note": {
			"type": "string"
		},
		"createdDate": {
			"type": "integer",
			"format": "timestamp"
		},
		"deletedDate": {
			"description": "Когда такс был удаленн пользователем",
			"type": "integer",
			"format": "timestamp"
		},
		"completedDate": {
			"type": "integer",
			"format": "timestamp"
		},
		"actions": {
			"description": "Спиоск действий которые необходимо выполнить по этому таску",
			"type": "array",
			"items": {
				"$ref": "#/definitions/Action"
			}
		},
		"duration": {
			"description": "Описания стратегии повторения действий по событию",
			"type": "object",
			"properties": {
				"startDate": {
					"description": "Дата первого события",
					"type": "integer",
					"format": "timestamp"
				},
				"strategy": {
					"description": "Стратегия повторения события",
					"enum": [ "daily", "monthly", "yearly", "weekdays", "interval" ]
				},
				"weekdays": {
					"description": "Дени недели в которые повторять событие",
					"type": "array",
					"items": {
						"type": "string",
						"enum": ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
					}
				},
				"interval": {
					"description": "Интервал через который повторить события в ms используется при стратегии interval",
					"type": "integer"
				},
				"count": {
					"description": "Общее количество actions, которое необходимо сгенерировать",
					"type": "integer"
				}
			}

		}
	},

	"required": ["ownerId", "milestoneId",  "duration"],

	"links": [
		{
			"description": "Владелец задачи",
			"rel": "owner",
			"href": "/users/{ownerId}"
		},
		{
			"description": "Родительская ценность",
			"rel": "parent",
			"href": "/users/{ownerId}/cores/{coreId}"
		},
		{
			"description": "Родительская привычка или цель",
			"rel": "parent",
			"href": "/users/{ownerId}/cores/{coreId}/milestone/{thingId}"
		},
		{
			"description": "Родительская привычка или цель",
			"rel": "parent",
			"href": "/users/{ownerId}/cores/{coreId}/milestone/{thingId}"
		}
	]


};
