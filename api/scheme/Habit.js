module.exports =  {
	"type": "object",
	"description": "Пользовательская привычка, имеет обязательного родителя - ценность или другая цель.",

	"properties": {
		"achieved": {
			"description": "Признак развитости привычки",
			"type": "boolean"
		},
		"avatar": {
			"description": "Ссылка на загруженную аватарку",
			"type": "string",
			"format": "url"
		},
		"name": {
			"description": "Сформулированная привычка",
			"type": "string"
		},
		"reason": {
			"description": "Информация, почему эта привычка важна пользователю",
			"type": "string"
		}
	},

	"required": ["name", "reason"]
};
