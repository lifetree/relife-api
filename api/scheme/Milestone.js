module.exports =  {
	"type": "object",
	"description": "Сущность представляет собой древовидную структуру, которая хранит связи между целям, ценностями и превычками",

	"properties": {
		"id": {
			"type": "integer"
		},
		"ownerId": {
			"type": "integer"
		},
		"coreId": {
			"type": "integer"
		},
		"parentId": {
			"description": "Ссылка на родительский элемент",
			"type": "integer"
		},
		"type": {
			"description": "Тип этапа (цель или привычка)",
			"type": "string",
			"enum": [ "habit", "goal" ]
		},
		"thing": {
			"type": "object",
			"oneOf": [
				{ "$ref": "#/definitions/Habit" },
				{ "$ref": "#/definitions/Goal" }
			]
		},
		"tasks": {
			"description": "Список задач пользователя",
			"type": "array",
			"items": {
				"$ref": "#/definitions/Task"
			}
		},
		"plannedDate": {
			"description": "Дата создания этапа",
			"type": "integer",
			"format": "timestamp"
		},
		"createdDate": {
			"description": "Дата создания этапа",
			"type": "integer",
			"format": "timestamp"
		},
		"deletedDate": {
			"description": "Дата удаления этапа",
			"type": "integer",
			"format": "timestamp"
		}
	},

	"required": ["type", "coreId", "thing", "plannedDate"],

	"links": [
		{
			"description": "Владелец привычки или цели",
			"rel": "owner",
			"href": "/users/{ownerId}"
		},
		{
			"description": "Родительская ценность",
			"rel": "parent",
			"href": "/users/{ownerId}/cores/{coreId}"
		},
		{
			"description": "Связанные с привычкой, периоды отслеживания",
			"rel": "have",
			"href": "/users/{ownerId}/cores/{coreId}/things/{id}/periods"
		}
	]

};

// users/1/things/1/canCompleted - > { res: true,  }

// users/1/things/?type=habit&isDeleted=true
// users/1/things/1/timings?status=progress

// timing (это некоторая расписание  на создания тасков для шедулера) status (new, progress, new)
// сервер может использовать timing для принятия решения о развитости habit (21 таск закрытый и completed)

// users/1/cores/ - списсок ценностей
// users/1/cores/1/goals/ - все цели пользователя (с учетом вложенных)
// POST users/1/cores/1/goals -> goalId -
// users/1/cores/1/plans/2/periods/ -> [ массив периодов ], []

