module.exports =  {
	"type": "object",
	"description": "Пользователь системы",

	"properties": {
		"id": {
			"type": "integer"
		},
		"name": {
			"type": "string"
		},
		"email": {
			"type": "string",
			"format": "email"
		},
		"pwd": {
			"description": "md5 hash от пароля и логина пользователя (login:pwd)",
			"type": "string",
			"format": "md5hash"
		},
		"birthDate": {
			"description": "дата рождения пользователя",
			"type": "integer",
			"format": "timestamp"
		},
		"createdDate": {
			"description": "Дата регистрации пользователя в системе",
			"type": "integer",
			"format": "timestamp"
		},
		"country": {
			"description": "Идентификатор страны проживания пользователя в формате 8859-5",
			"type": "string",
			"format": "country-id"
		},
		"city": {
			"description": "Название города",
			"type": "string"
		},
		"gender": {
			"description": "Пол, 1 - мужской, 0 - женский",
			"type": "integer"
		},
		"occupation": {
			"description": "Профессия",
			"type": "string"
		},
		"phone": {
			"description": "Телефон пользователя",
			"type": "string",
			"format": "phone"
		},
		"skype": {
			"description": "Скайп пользователя",
			"type": "string"
		}
	},

	"required": ["name", "email", "pwd" ]
};
