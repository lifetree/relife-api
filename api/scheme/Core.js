module.exports =  {
	"type": "object",
	"description": "Ценность является корневым элементом для всех остальных сущностей." +
	" Никакая сущность (цель, привычка, задача) не может быть создана в отрыве от ценности.",

	"properties": {
		"ownerId": {
			"type": "integer"
		},
		"id": {
			"type": "integer"
		},
		"name": {
			"type": "string"
		},
		"reason": {
			"description": "Информация, почему эта ценность важна пользователю",
			"type": "string"
		},
		"avatar": {
			"description": "Ссылка на загруженную аватарку",
			"type": "string",
			"format": "url"
		},
		"color": {
			"description": "Цвет ценности",
			"type": "string",
			"format": "hex-color"
		},
		"createdDate": {
			"type": "integer",
			"format": "timestamp"
		},
		"deletedDate": {
			"type": "integer",
			"format": "timestamp"
		}
	},

	"required": ["name", "ownerId", "color"],

	"links": [
		{
			"rel": "owner",
			"href": "/users/{ownerId}"
		}
	]

};
