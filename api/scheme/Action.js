module.exports =  {
	"type": "object",
	"description": "Действия которое необходимо выполнить пользователю.",

	"properties": {
		"start": {
			"description": "Дата когда действие должно начато",
			"type": "integer",
			"format": "timestamp"
		},
		"failed": {
			"description": "Признак того, что действие зафейленно",
			"type": "boolean"
		},
		"completed": {
			"description": "Дата завершения данного действия",
			"type": "integer",
			"format": "timestamp"
		}
	},

	"required": ["taskId", "start"]
};
