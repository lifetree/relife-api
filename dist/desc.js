define(function () {
	return {
		"resources": {
			"session": {"GET*": ""},
			"users": {
				"POST*": "",
				"{userid}": {
					"GET*#/userid": "",
					"PATCH*#/userid": "",
					"cores": {
						"GET*#/{userid=userid}": "",
						":list": "GET",
						"POST*#/{userid=userid}": "",
						"{coreid}": {"GET*#/{userid=userid}#/coreid": "", "PATCH*#/{userid=userid}#/coreid": ""}
					},
					"milestones": {
						"GET*#/{userid=userid}": "",
						":list": "GET",
						"POST*#/{userid=userid}": "",
						"{mid}": {"GET*#/{userid=userid}#/mid": "", "PATCH*#/{userid=userid}#/mid": ""}
					}
				}
			}
		}, "base": "//relifeapi-imagemap.rhcloud.com/api"
	}
});
