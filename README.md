## Проект LifeTree

## Установка
Для лоакльной работы необходима установлен и запущен сервер mongodb.
После

```
	npm install
	node server/app
```

## Настройка

 * src/configs/app.{тип сервера} - настройки приложения (dev - разработка, prod - бой)
 * src/configs/db - настройки базы данных

## Миграция

	./migrate.sh dev up

где `dev` - это тип окружения (dev|prod) и `up` (это куда идти вверх или вниз)

## Создание новой миграции

	node ./node_modules/mongodb-migrate/bin/mongo-migrate.js -runmm create milestones

где `milestones` - лейбл новой миграции


## Тестирование

```
	node server/app
 	https://localhost:8080/tests/
```


## Журналирование
Используется сервис https://relife.loggly.com, домен relife
Для запуска на много
```
curl -O https://www.loggly.com/install/configure-file-monitoring.sh
sudo bash configure-file-monitoring.sh -a relife -u burlak -f $OPENSHIFT_MONGODB_DB_LOG_DIR -l mongodb -tag MongoDB
```


