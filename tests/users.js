define(function(require) {
	var QUnit = window.QUnit,
		apic = require('../client/apic'),
		desc = require('../client/descriptor'),
		api = apic(desc);

	window.api = api;

	var next = QUnit.start.bind(QUnit),
		test = QUnit.asyncTest.bind(QUnit);

	QUnit.module('users', function (hooks) {

		hooks.beforeEach(function (assert) {
			api.unauthorize();
			api.augment('X-Test-Name', encodeURI(assert.test.testName));
		});


		test('sessions: проверка не корректной сесии сесесси', function( assert ) {
			api.authorize.basic('test@r1233e.life', 'test');

			api.session.get(function (err) {
				assert.equal(err && err.status, 401, 'ошибка авторизация');
				next();
			})

		});

		test('sessions: проверка получения сесесси', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.session.get(function (err, res) {
				assert.equal(err, null, 'без ошибок');
				assert.equal(res.id, 1, 'корректный id');
				assert.equal(!!res.token, true, 'в ответе есть токен');

				api.authorize.token(res.token);

				// должен сработать без параметров
				api.users.get(function (err, res) {
					assert.equal(err, null, 'без ошибок');
					assert.equal(res.id, 1, 'профиль получен');
					next();
				});

			});

		});


		test('получаем собственный профиль', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.get({userid: 1}, function (err, res) {
				assert.equal(err, null, 'без ошибок');
				assert.equal(res.id, 1, 'корректный id');
				assert.equal(res.email, 'test@re.life', 'корректный email');
				next();
			});

		});

		test('пользователь не может получать чужие профили', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.get({userid: 2}, function (err, res) {
				assert.equal(err.status, 403, 'профиль не доступен');
				assert.equal(res, null, 'ответ пустой');
				next();
			});

		});


		test('создание нового пользователя', function( assert ) {
			var email = "test" +  Date.now() +"@email.ru",
				pwd = 'adsads';

			api.users.post({name: "need_remove", email: email, pwd: pwd}, function (err, user) {
				assert.equal(err, null, 'без ошибок');
				assert.equal(typeof user.id, 'number', 'овтет должен содержать id нового пользователя');

				// проверям что новый пользователь может авторизоватся
				api.authorize.basic(email, pwd);

				api.users.get({userid: user.id}, function (err, res) {
					assert.equal(err, null, 'без ошибок');
					assert.equal(user.id, res.id, 'вернул вновь созданного пользоватлея');

					next();
				});

			});

		});

		test('модификация профиля', function( assert ) {
			var name = 'test' + Date.now();

			api.authorize.basic('test@re.life', 'test');

			api.users.patch({userid: 1}, {name: name}, function (err) {
				assert.equal(err, null, 'без ошибок');

				api.users.get({userid: 1}, function (err, res) {
					assert.equal(res.name, name, 'имя успешно измененно');
					next();
				});


			});

		});

		test('создание пользователя с некорректными параметрами', function (assert) {

			api.users.post({name: "need_remove", email: 'adsads'}, function (err) {
				assert.equal(!!err, true, 'ошибка созданиея');
				assert.equal(Object.keys(err).length, 3, 'не достает пароля и email не корректен');
				assert.equal(err.email, 'invalid', 'email не валидный');
				assert.equal(err.pwd, 'required', 'пароль не указан');
				next();
			});

		});


	});



});
