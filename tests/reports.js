define(function(require) {
	var QUnit = window.QUnit,
		apic = require('../client/apic'),
		desc = require('../client/descriptor'),
		api = apic(desc);

	window.api = api;

	var next = QUnit.start.bind(QUnit),
		test = QUnit.asyncTest.bind(QUnit);

	QUnit.module('reports', function (hooks) {

		hooks.beforeEach(function (assert) {
			api.unauthorize();
			api.augment('X-Test-Name', encodeURI(assert.test.testName));
		});

		test('проверяем выборку по диапазону дат', function( assert ) {
			api.authorize.basic('reports@re.life', 'test');

			api.users.tasks.list({userid: 4, start: 2, end: 2}, function (err, res){
				assert.equal(err, null, 'нет ошибок');
				assert.equal(res.length, 1, 'найден 1 отчет');
				assert.equal(res[0] && res[0].id, 4, 'найденн правильный отчет');
				next();
			});

		});

		test('таски должне быть отсартированы от даты start', function( assert ) {
			api.authorize.basic('reports@re.life', 'test');

			api.users.tasks.list({userid: 4, start: 1}, function (err, res){
				assert.equal(err, null, 'нет ошибок');
				assert.equal(res.length, 3, 'найден 2 отчета');
				assert.equal(res[2].id, 5, 'в конце отчет с самым стары');
				next();
			});

		});

		test('если передан failed то показываются только зафлейные таски', function( assert ) {
			api.authorize.basic('reports@re.life', 'test');

			api.users.tasks.list({userid: 4, failed: true, start: 1}, function (err, res){
				assert.equal(err, null, 'нет ошибок');
				assert.equal(res.length, 1, 'найден 2 отчета');
				assert.equal(res[0] && res[0].id, 4, 'найденн правильный отчет');
				next();
			});

		});

		test('если передан failed то показываются только завершенные таски', function( assert ) {
			api.authorize.basic('reports@re.life', 'test');

			api.users.tasks.list({userid: 4, completed: true, start: 1}, function (err, res){
				assert.equal(err, null, 'нет ошибок');
				assert.equal(res.length, 1, 'найден 2 отчета');
				assert.equal(res[0] && res[0].id, 6, 'найденн правильный отчет');
				next();
			});

		});

	});

});
