define(function(require) {
	var QUnit = window.QUnit,
		apic = require('../client/apic'),
		desc = require('../client/descriptor'),
		api = apic(desc);

	window.api = api;

	var next = QUnit.start.bind(QUnit),
		test = QUnit.asyncTest.bind(QUnit);

	function make_base_auth(user, password) {
		var tok = user + ':' + password;
		var hash = btoa(tok);
		return "Basic " + hash;
	}

	function b64toBlob(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, {type: contentType});
		return blob;
	}

	QUnit.module('photos', function (hooks) {

		hooks.beforeEach(function (assert) {
			api.unauthorize();
			api.augment('X-Test-Name', encodeURI(assert.test.testName));
		});

		test('photos: загрузка фотки', function( assert ) {

			$.get('/tests/miscs/photo.base64', function (base64) {
				var blob = b64toBlob(base64, 'image/png'),
					fd = new FormData();

				fd.append('fname', 'black-mister.png');
				fd.append('photo', blob);

				$.ajax({
					type: 'POST',
					url: '/api/users/1/photos',
					data: fd,
					processData: false,
					contentType: false,
					beforeSend: function (xhr){
						xhr.setRequestHeader('Authorization', make_base_auth('test@re.life', 'test'));
					}
				}).done(function() {
					assert.equal(true, true, 'фотка успещно загрженна');
					next();
				});

			});

		});


		test('photos: получения списка фоток', function( assert ) {
			api.authorize.basic('xuy@re.life', 'test');

			api.users.photos.list({userid: 3}, function (err, res) {
				assert.equal(err, null, 'список фоток успешно получен');
				assert.equal(res.length > 0, true, 'есть фотки');
				next();
			});

		});

		test('photos: удаление фотографии', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			$.get('/tests/miscs/photo.base64', function (base64) {
				var blob = b64toBlob(base64, 'image/png'),
					fd = new FormData();

				fd.append('fname', 'black-mister.png');
				fd.append('photo', blob);

				$.ajax({
					type: 'POST',
					url: '/api/users/1/photos',
					data: fd,
					processData: false,
					contentType: false,
					beforeSend: function (xhr){
						xhr.setRequestHeader('Authorization', make_base_auth('test@re.life', 'test'));
					}
				}).done(function() {

					api.users.photos.list({userid: 1}, function (err, res) {
						assert.equal(err, null, 'список фоток успешно получен');
						assert.equal(res.length > 0, true, 'есть фотки');

						api.users.photos.delete({userid: 1, photo: res[0]}, {}, function (err, res) {
							assert.equal(err, null, 'фотка успешно удалена');
							console.log(res);
							next();
						});


					});

				});

			});




		});



	});

});
