define(function(require) {
	var QUnit = window.QUnit,
		apic = require('../client/apic'),
		desc = require('../client/descriptor'),
		api = apic(desc);

	window.api = api;

	var next = QUnit.start.bind(QUnit),
		test = QUnit.asyncTest.bind(QUnit);

	var getBodyStub = function () {

		return {
			"coreId": "1",
			"plannedDate":  Date.now() + 1000000,
			"type": "habit",
			"thing": {
				"avatar": "http://engenhariae.com.br/wp-content/uploads/2015/06/cores-capacetes-significados1-950x534.jpg",
				"name": "нужно начать бегать",
				"reason": "а просто захотелось добавить"
			},

			"tasks": [{

				duration: {
					strategy: 'daily',
					startDate: Date.now()
				}

			}]
		};

	};


	QUnit.module('tasks', function (hooks) {


		hooks.beforeEach(function (assert) {
			api.unauthorize();
			api.augment('X-Test-Name', encodeURI(assert.test.testName));
		});

		test('получаем один таск', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.tasks.get({userid: 1, mid: 1, taskid: 3}, function (err, res) {
				assert.equal(err, null, 'без ошибок');
				assert.equal(res.actions && res.actions.length, 3, 'есть три действия');
				assert.equal(res.actions[2].completed, true, 'есть признак заверщенности');
				next();
			});

		});

		test('пробуем обновить имя у таска', function( assert ) {
			var name = 'test' + Date.now();

			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.tasks.patch({userid: 1, mid: 1, taskid: 3}, {name: name}, function (err) {
				assert.equal(err, null, 'без ошибок');

				api.users.milestones.tasks.get({userid: 1, mid: 1, taskid: 3}, function (err, res) {
					assert.equal(err, null, 'без ошибок');
					assert.equal(res && res.name, name, 'имя успешно измененно');
					next();
				});

			});

		});


		test('проверяем логику работы тасков с пеналти', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			var ms = getBodyStub(),
				action;

			ms.type = "habbit";
			ms.thing.criteria = 'сдал экзмен';
			ms.thing.plan = 'готовится тщательно';

			api.users.milestones.post({userid: 1}, ms, function (err, ms) {
				assert.equal(err, null, 'нет ошибок');

				api.users.milestones.tasks.post({userid: 1, mid: ms.id}, {
					duration: {
						strategy: 'daily',
						startDate: Date.now(),
						count: 21
					},
					penalized: true
				}, function (err, task) {
					assert.equal(err, null, 'нет ошибок');

					action = task.actions[0];
					action.failed = true;

					api.users.milestones.tasks.patch({taskid: task.id, userid: 1, mid: ms.id}, { actions: [action] }, function (err) {
						assert.equal(err, null, 'нет ошибок');

						api.users.milestones.tasks.get({userid: 1, mid: ms.id, taskid: task.id}, function (err, task) {
							assert.equal(err, null, 'нет ошибок');
							assert.equal(task.actions.length, 24, 'должен быть добавленно 3 дополнительных таска');
							next();
						});

					});

				});


			});


		});

		test('проверяем логику работы рекурентных тасков', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			var ms = getBodyStub(),
				action;

			ms.type = "goal";
			ms.thing.criteria = 'сдал экзмен';
			ms.thing.plan = 'готовится тщательно';

			api.users.milestones.post({userid: 1}, ms, function (err, ms) {
				assert.equal(err, null, 'нет ошибок');

				api.users.milestones.tasks.post({userid: 1, mid: ms.id}, {
					duration: {
						strategy: 'daily',
						startDate: Date.now(),
						count: 1
					},
					recurrent: true
				}, function (err, task) {
					assert.equal(err, null, 'нет ошибок');

					action = task.actions[0];
					action.completed = true;

					api.users.milestones.tasks.patch({taskid: task.id, userid: 1, mid: ms.id}, { actions: [action] }, function (err) {
						assert.equal(err, null, 'нет ошибок');

						api.users.milestones.tasks.get({userid: 1, mid: ms.id, taskid: task.id}, function (err, task) {
							assert.equal(err, null, 'нет ошибок');
							assert.equal(task.actions.length, 2, 'должен быть создан новый таск');
							assert.equal(new Date(task.actions[1].start).getDate() - new Date(task.actions[0].start).getDate(), 1, 'должна быть разница в день');
							next();
						});

					});

				});


			});


		});

		test('проверяем генерацию тасков с интеревалом daily', function( assert ) {
			api.authorize.basic('test@re.life', 'test');
			var ms = getBodyStub();

			api.users.milestones.post({userid: 1}, ms, function (err, ms) {
				assert.equal(err, null, 'нет ошибок');

				api.users.milestones.tasks.post({userid: 1, mid: ms.id}, {
					duration: {
						strategy: 'daily',
						startDate: Date.now(),
						count: 2
					}
				}, function (err, task) {
					assert.equal(err, null, 'нет ошибок');

					api.users.milestones.tasks.get({userid: 1, mid: ms.id, taskid: task.id}, function (err, task) {
						var actions = task.actions;

						assert.equal(err, null, 'ошибок нет');
						assert.equal(actions  && actions.length, 2, 'созданно 2 action');

						assert.equal(actions[1].start - actions[0].start, 86400000, 'разница ровно сутки');
						next();
					});

				});


			});

		});

		test('проверяем генерацию тасков с интеревалом yearly', function( assert ) {
			api.authorize.basic('test@re.life', 'test');
			var ms = getBodyStub();

			api.users.milestones.post({userid: 1}, ms, function (err, ms) {
				assert.equal(err, null, 'нет ошибок');

				api.users.milestones.tasks.post({userid: 1, mid: ms.id}, {
					duration: {
						strategy: 'yearly',
						startDate: Date.now(),
						count: 2
					}
				}, function (err, task) {
					assert.equal(err, null, 'нет ошибок');

					api.users.milestones.tasks.get({userid: 1, mid: ms.id, taskid: task.id}, function (err, task) {
						var actions = task.actions;

						assert.equal(err, null, 'ошибок нет');
						assert.equal(actions  && actions.length, 2, 'созданно 2 action');

						assert.equal(new Date(actions[1].start).getYear() - new Date(actions[0].start).getYear(), 1, 'разница ровно сутки');
						next();
					});

				});


			});

		});



	});

});
