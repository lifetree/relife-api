define(function(require) {
	var QUnit = window.QUnit,
		apic = require('../client/apic'),
		desc = require('../client/descriptor'),
		api = apic(desc);

	window.api = api;

	var next = QUnit.start.bind(QUnit),
		test = QUnit.asyncTest.bind(QUnit);


	var getBodyStub = function () {

		return {
			"coreId": "1",
			"plannedDate":  Date.now() + 1000000,
			"type": "habit",
			"thing": {
				"avatar": "http://engenhariae.com.br/wp-content/uploads/2015/06/cores-capacetes-significados1-950x534.jpg",
				"name": "нужно начать бегать",
				"reason": "а просто захотелось добавить"
			}
		};

	};

	QUnit.module('milestones', function (hooks) {

		hooks.beforeEach(function (assert) {
			api.unauthorize();
			api.augment('X-Test-Name', encodeURI(assert.test.testName));
		});


		test('получения списка пользовательских привычек и ценностей', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 1}, function (err, res) {
				assert.equal(err, null, 'ошибок нет');
				assert.equal(res.length > 0, true, 'ответ не пустой');
				next();
			});

		});

		test('получения списка пользовательских целей', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 1, type: 'goal'}, function (err, res) {
				assert.equal(err, null, 'ошибок нет');

				res.forEach(function (item) {
					assert.equal(item.type, 'goal', 'ответ не пустой');
				});

				next();
			});

		});


		test('пользователь не может получать чужие превчки и цели', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 2}, function (err, res) {
				assert.equal(err && +err.status === 403, true, 'ошибок авторизации');
				next();
			});

		});

		test('получаем привычки по id родителя', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 1, parent: 1}, function (err, res) {
				assert.equal(err, null, 'ошибок авторизации');
				assert.equal(res && res.length, 1, 'найдены две дочерние задачи');
				assert.equal(res[0].id, 2, 'корректный id');
				next();
			});

		});

		test('создания  новой майлстоуна', function( assert ) {
			api.authorize.basic('test@re.life', 'test');
			var ms = getBodyStub();

			api.users.milestones.post({userid: 1}, ms, function (err, res) {
				assert.equal(err, null, 'ошибок нет');
				next();
			});

		});

		test('удаление  майлстоуна', function( assert ) {
			api.authorize.basic('test@re.life', 'test');
			var ms = getBodyStub();

			api.users.milestones.post({userid: 1}, ms, function (err, res) {
				assert.equal(err, null, 'ошибок нет');

				api.users.milestones.delete({userid: 1, mid: res.id},{},  function (err) {
					assert.equal(err, null, 'milestone удален');

					api.users.milestones.get({userid: 1, mid: res.id}, function (err, ms) {
						assert.equal(err,null, 'milestone найден ' + res.id);
						assert.equal(!!ms.deletedDate, true, 'у мейлстоуна проставленна дата удаления');

						next();
					});

				});

			});

		});


		test('нельзя создать майлстон если не заполнены все обятельные параметры', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			var ms = {

				"coreId": "1",
				"type": "habit",
				"thing": {
					"avatar": "http://engenhariae.com.br/wp-content/uploads/2015/06/cores-capacetes-significados1-950x534.jpg"
				}

			};

			api.users.milestones.post({userid: 1}, ms, function (err, res) {
				assert.equal(err && err.status === 400, true, 'ошибка валидации');
				assert.equal(err && err.thing.name, 'required', 'ошибока');
				assert.equal(err && err.thing.reason, 'required', 'ошибока');
				next();
			});

		});



		test('если у пользователя нет ценности, нельзя давать возможность создавать для нее milestones', function( assert ) {
			api.authorize.basic('test@re.life', 'test');
			var ms = getBodyStub();

			delete ms.coreId;

			api.users.milestones.post({userid: 1}, ms, function (err) {
				assert.equal(err && err.status === 400, true, 'ошибока');
				assert.equal(err && err.coreId, 'not_exist', 'ошибока');
				next();
			});

		});


		test('получения пользовательского майлстоуна', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.get({userid: 1, mid: 1}, function (err, res) {
				assert.equal(err, null, 'ошибок нет');
				assert.equal(+res.id, 1);
				assert.equal(+res.ownerId, 1);
				assert.equal(res.type, 'habit');
				assert.equal(typeof res.thing, 'object');

				next();
			});

		});


		test('обновление пользовательского мейлстоуна', function( assert ) {
			var text = 'test name' + Date.now();

			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.patch({userid: 1, mid: 1}, { thing: { name: text } }, function (err, res) {
				assert.equal(err, null, 'ошибок нет');

				api.users.milestones.get({userid: 1, mid: 1}, function (err, res) {
					assert.equal(err, null, 'ошибок нет');
					assert.equal(res.thing.name, text);
					next();
				});

			});

		});

		test('полученние удаленных мейлстоунов', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 1, deleted: true}, function (err, list) {
				var isCorrect = true;

				assert.equal(err, null, 'ошибок нет');
				assert.equal(list && list.length > 0, true, 'есть удаленные мейлстоуны');


				for (var i=0; i < list.length; i++) {
					var item = list[i];

					isCorrect = !!item.deletedDate;

					if (!isCorrect) {
						break;
					}

				}

				assert.equal(isCorrect, true, 'только удаленные мейлстоуны');
				next();
			});

		});

		test('полученние завершенных мейлстоунов', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 1, completed: true}, function (err, list) {
				var isCorrect = true;

				assert.equal(err, null, 'ошибок нет');
				assert.equal(list && list.length > 0, true, 'есть завершенные мейлстоуны');


				for (var i=0; i < list.length; i++) {
					var item = list[i];

					isCorrect = !!item.completedDate;

					if (!isCorrect) {
						break;
					}

				}

				assert.equal(isCorrect, true, 'только завершенные мейлстоуны');
				next();
			});

		});


		test('полученние мейлстоунов для заданой ценности (core)', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.milestones.list({userid: 1, cores: '1,3'}, function (err, list) {
				var correct;

				assert.equal(err, null, 'ошибок нет');
				assert.equal(list && list.length > 0, true, 'список не пустой');

				correct = list.filter(function (ms) {
					return [1,3].indexOf(+ms.coreId) !== -1;
				});

				assert.equal(correct.length, list.length, 'все мейлстоуны из заданного кора');
				next();
			});

		});


	});



});
