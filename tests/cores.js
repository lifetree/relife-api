define(function(require) {
	var QUnit = window.QUnit,
		apic = require('../client/apic'),
		desc = require('../client/descriptor'),
		api = apic(desc);

	window.api = api;

	var next = QUnit.start.bind(QUnit),
		test = QUnit.asyncTest.bind(QUnit);


	QUnit.module('cores', function (hooks) {

		hooks.beforeEach(function (assert) {
			api.unauthorize();
			api.augment('X-Test-Name', encodeURI(assert.test.testName));
		});

		test('получения списка пользовательских целей', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.cores.list({userid: 1}, function (err, res) {
				assert.equal(err, null, 'ошибок нет');
				assert.equal(res.length > 0, true, 'ответ не пустой');
				next();
			});

		});

		test('пользователь не должен иметь доступ до чужих целей', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.cores.list({userid: 2}, function (err, res) {
				assert.equal(!!err, true, 'профиль не доступен');
				next();
			});

		});


		test('создание новой цели', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.cores.post({userid: 1}, {name: "первая тестовая" + Date.now(), color: 'fsfsafs'}, function (err, res) {
				assert.equal(err, null, 'цель успешно создана');
				next();
			});

		});

		test('удаление цели', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.cores.post({userid: 1}, {name: "первая тестовая" + Date.now(), color: 'fsfsafs'}, function (err, res) {
				assert.equal(err, null, 'цель успешно создана');

				api.users.cores.delete({userid: 1, coreid: res.id}, {}, function (err) {
					assert.equal(err, null, 'цель успешно удаленна');

					api.users.cores.get({userid:1, coreid: res.id}, function (err) {
						assert.equal(err && err.status, 404, 'цель не найденна ' + res.id);
						next();
					});

				});

			});

		});


		test('получение цели', function( assert ) {
			api.authorize.basic('test@re.life', 'test');

			api.users.cores.get({userid: 1, coreid: 2}, function (err, res) {
				assert.equal(err, null, 'цель успешно получена');
				assert.equal(+res.id, 2, 'цель успешно получена');
				assert.equal(+res.ownerId, 1, 'цель успешно получена');
				next();
			});

		});



		test('изменение цели', function( assert ) {
			var text = Date.now() + 'test';

			api.authorize.basic('test@re.life', 'test');

			api.users.cores.patch( {userid: 1, coreid: 2}, {name:text}, function (err, res) {
				assert.equal(err, null, 'цель успешно обновлена');

				api.users.cores.get( {userid: 1, coreid: 2}, function (err, res) {
					assert.equal(err, null, 'цель успешно получена');
					assert.equal(res.name, text);
					next();
				});


			});

		});

	});



});
