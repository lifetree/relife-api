var bootprint = require('bootprint'),
	env = require('./src/libs/env'),
	api = require('./api/api');

var spec = api.getSpec(env.getConfig());

bootprint
	.load(require('bootprint-swagger'))
	.build(spec, './doc')
	.generate()
	.done(console.log);
