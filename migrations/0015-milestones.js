var async = require('async'),
	data = require('./data/milestones');

exports.up = function(db, done){
	var milestones = db.collection('milestones'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			milestones.insert(item, next);
		};

	});

	async.parallel(flow, done);
};

exports.down = function(db, done){
	var milestones = db.collection('milestones'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			milestones.remove({id: item.id}, next);
		};

	});

	async.parallel(flow, done);
};
