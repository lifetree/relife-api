var async = require('async'),
	data = require('./data/tasks');

exports.up = function(db, done){
	var tasks = db.collection('tasks'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			tasks.insert(item, next);
		};

	});

	async.parallel(flow, done);
};

exports.down = function(db, done){
	var tasks = db.collection('tasks'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			tasks.remove({id: item.id}, next);
		};

	});

	async.parallel(flow, done);
};
