var async = require('async'),
	data = require('./data/cores');

exports.up = function(db, done){
	var cores = db.collection('cores'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			cores.insert(item, next);
		};

	});

	async.parallel(flow, done);
};

exports.down = function(db, done){
	var cores = db.collection('cores'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			cores.remove({id: item.id}, next);
		};

	});

	async.parallel(flow, done);
};
