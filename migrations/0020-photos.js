var async = require('async'),
	data = require('./data/photos');

exports.up = function(db, done){
	var photos = db.collection('photos'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			photos.insert(item, next);
		};

	});

	async.parallel(flow, done);
};

exports.down = function(db, done){
	var photos = db.collection('photos'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			photos.remove({id: item.id}, next);
		};

	});

	async.parallel(flow, done);
};
