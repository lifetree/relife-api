var md5 = require('md5'),
	async = require('async'),
	data = require('./data/users');

exports.up = function(db, done){
	var users = db.collection('users'),
		flow;

	flow = data.map(function (item) {
		item.pwd = md5(item.pwd);

		return function (next) {
			users.insert(item, next);
		};

	});

	async.parallel(flow, done);
};

exports.down = function(db, done){
	var users = db.collection('users'),
		flow;

	flow = data.map(function (item) {

		return function (next) {
			users.remove({id: item.id}, next);
		};

	});

	async.parallel(flow, done);
};
