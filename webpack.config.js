'use strict';

var path = require('path'),
    webpack = require('webpack');

var output = {
    path: './tests',
    filename: '[name].js',
    publicPath: '/',
    library: 'api',
    libraryTarget: 'umd',
    umdNamedDefine: true
};

var externals = [
	function filter(context, request, cb) {
		var isExternal =
			request.match(/^[a-z][a-z\/\.\-0-9]*$/i);
		cb(null, Boolean(isExternal));
	}
];


var prod = {
    name: 'api',
    context: path.resolve(__dirname),

    entry: {
        api: './dist/api.js'
    },

    output: output,
	externals: externals,

    module: {

        loaders: [
            {
				test: /\.js$/,
                loader: 'babel'
            }
        ]

    },

    plugins: [
    ]

};

module.exports = [prod];
