var utils = require('../../src/libs/utils');

describe('libs/utils', function() {

	describe('getMetaByUri', function () {

		beforeEach(function () {
			this.spec = { paths: {
				'/users/{id}': {
					'get': {
						grants:{
							'owner': {
								'in': 'params',
								name: 'id',
								session: 'id'
							}
						},
						"__controller": "user.get",
						"security": [{ basicAuth: []}],
						"__validateSchema": "Test"
					}

				},
				'/users': {
					"post": {
						"__controller": "user.create",
						"__validateSchema": "User"
					}
				},

				'/users/{id}/cores/{coreid}': {
					'get': {
						grants:{
							'owner': {
								'in': 'params',
								name: 'id',
								session: 'id'
							}
						},
						"__controller": "core.get",
						"security": [{ basicAuth: []}],
						"__validateSchema": "Test"
					}
				}

			}};
		});

		it ('успешное полечение мета информации о user/{id}', function () {
			var meta = utils.getMetaByUri(this.spec, 'users/1', 'get');

			expect(meta.controller).toBe('user');
			expect(meta.route).toBe('/users/{id}');
			expect(meta.action).toBe('get');
			expect(meta.security).toEqual(['basicAuth']);

			expect(meta.grants).toEqual({'owner': {
				'in': 'params',
				name: 'id',
				session: 'id'
			}});

			expect(meta.schema).toBe('Test');
			expect(meta.expressRoute).toBe('/users/:id');
		});

		it ('успешное полечение мета информации о user/{id}/core/{coreid}', function () {
			var meta = utils.getMetaByUri(this.spec, 'users/1/cores/2', 'get');

			expect(meta.controller).toBe('core');
			expect(meta.route).toBe('/users/{id}/cores/{coreid}');
			expect(meta.action).toBe('get');
			expect(meta.security).toEqual(['basicAuth']);

			expect(meta.grants).toEqual({'owner': {
				'in': 'params',
				name: 'id',
				session: 'id'
			}});

			expect(meta.schema).toBe('Test');
			expect(meta.expressRoute).toBe('/users/:id/cores/:coreid');
		});


	});


});

