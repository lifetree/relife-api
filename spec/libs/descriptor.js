var descriptor = require('../../src/libs/descriptor');

describe('libs/descriptor', function() {

	describe('transform', function () {

		beforeEach(function () {
			this.spec = { paths: {

				'/users/{id}/cores/{coreid}': {
					'get': {
						grants:{
							'owner': {
								'in': 'params',
								name: 'id',
								session: 'id'
							}
						},
						"__controller": "core.get",
						"security": [{ basicAuth: []}],
						"__validateSchema": "Test",
						"__alias": "one"
					}
				},
				'/users/{id}/milestones': {
					'get': {
						grants:{
							'owner': {
								'in': 'params',
								name: 'id',
								session: 'id'
							}
						},
						"__controller": "core.get",
						"security": [{ basicAuth: []}],
						"__validateSchema": "Test",
						"__alias": "list"
					}
				}

			}};
		});

		it ('успешная генерация дескрптора для ресурсов первого уровня (/users/{id})', function () {
			var result = descriptor.generate(this.spec.paths);
			expect(JSON.stringify(result)).toBe('{"users":{"GET*":"","{id}":{"GET*#/{id}":"","cores":{"GET*#/{id}":"","{coreid}":{"GET*#/{id}#/{coreid}":"",":one":"GET"}},"milestones":{"GET*#/{id}":"",":list":"GET"}}}}');
		});


	});


});

