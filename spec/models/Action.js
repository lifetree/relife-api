var Action = require('../../src/models/Action');

describe('models/Action', function() {

	describe('generate', function () {

		it ('проверяем генерирование тасков для daily стратегии', function () {
			var req = jasmine.createSpyObj('req', ['log']),
				task;

			var dt = new Date(1462186778567);

			task = {
				duration: {"strategy":"daily","startDate":dt.getTime (), count: 2},
				actions: []
			};

			Action.generate(task, req);

			expect(dt.getDay() + 1).toBe(new Date(task.actions[0].start).getDay());
			expect(dt.getDay() + 2).toBe(new Date(task.actions[1].start).getDay());
		});


	});


});

