// Gruntfile.js 
module.exports = function (grunt) {

	grunt.config.init({

		shell: {

			migration_create: {
				command: './node_modules/.bin/'
			},

			jasmine: {
				command: './node_modules/.bin/jasmine'
			}
		},


		requirejs: {
			compile: {
				options: {
					baseUrl: "./client",
					name: 'api',
					out: "./dist/api.js",
					optimize: "none",
					packages: [
						{
							"name": "apic",
							"location": "../node_modules/apic.js",
							"main": "lib/apic.js"
						}
					],
					onBuildWrite: function (moduleName, path, contents) {
						console.log(contents)
						return contents.replace(/'\.\//g, '\'apic/lib\/');
					}

				}
			}
		}


	});

	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-contrib-requirejs');

	grunt.registerTask('test', [
		'shell:jasmine'
	]);

};
