var express = require('express'),
	path = require('path'),
	https = require('https'),
	env = require('./libs/env'),
	api = require('../api/api'),
	quip = require('quip'),
	utils = require('./libs/utils'),
	uid = require('uid'),
	bodyParser = require('body-parser'),
	fileUpload = require('express-fileupload');


//logs
var winston = require('winston');
require('winston-loggly');

winston.add(winston.transports.Loggly, {
	token: 'c39a5b28-c525-4a37-a00f-fb8d8cdeecc8',
	subdomain: 'relife',
	tags: ['api'],
	json: true
});

var WINSTON_LEVELS = {
	'log': 'info',
	'fatal': 'error'
};

var config = env.getConfig(),
	spec = api.getSpec(env.getConfig()),
	app = express();

// добавлеяем новый метод коносли для журналирования ошибок
console.fatal = console.error;

// добавляем екстра методы для работы с запросами и ответами
app.use(quip);

// журналирование
app.use(function (req, res, next) {
	req.uid = uid();

	function processArg(arg) {
		var str = '';

		if (typeof arg === 'object') {


			for(var key in arg) {
				str += key + '=' + JSON.stringify(arg[key]) + ',';
			}

			arg = '(' + str.replace(/,$/, '') + ')';
		}

		return arg;
	}

	['log', 'fatal'].forEach(function (level) {

		req[level] = function () {
			var args = Array.prototype.slice.call(arguments),
				strs = Array.prototype.slice.call(arguments);

			strs.unshift(level.toUpperCase() + ':');
			strs.unshift('[uid:' + req.uid + ']');
			strs.unshift('[' + req.method + ':' + req.url + ']');

			strs = strs.map(processArg);
			args = args.map(processArg);

			env.isProd() && winston.log(WINSTON_LEVELS[level], {
				uid: req.uid,
				method: req.method,
				url: req.url,
				args: args
			});

			console[level].apply(console, strs);
		};

	});

	res.headers({'X-Req-Uid': req.uid});
	next();
});

// обрабатываем ошибки сервера
app.use(function(err, req, res, next) {
	req.fatal('server error', err);
	next();
});

app
	.use(bodyParser.json())
	.use(require('./services/meta')(spec, config))
	.use(require('./services/cors')(config.cors))
	.use(require('./services/validator')(spec.definitions))
	.use(require('./services/auth/main')())
	.use(fileUpload());

app.get('/api', require('./controllers/root')(spec));

// регестрируем описаные ресурсы
for (var key in spec.paths) {

	(function (uri) {

		Object.keys(spec.paths[uri]).forEach(function (method) {
			var meta = utils.getMetaByUri(spec, uri, method),
				unit;

			if (!meta.controller) {
				throw new Error('Дле метода ресурса не определен ни один контроллер ' + uri + ':' + method);
			}

			unit = require('./controllers/' + meta.controller)(spec);
			app[method](config.api.basePath + meta.expressRoute, unit[meta.action]);
		});

	})(key);

}

app.get('/env', function (req, res) {
	res.text(JSON.stringify(process.env, 1, 1) );
});

var apiDesc = require('./controllers/descriptor')(spec);

app.use('/client', express.static(path.resolve(require.resolve('apic.js'), '../')));
app.get('/client/descriptor.js', apiDesc.descriptor);
app.get('/api.js', apiDesc.client);

app.use('/', express.static('doc'));
app.use('/tests', express.static('tests'));
app.use('/tests', express.static('client'));
app.use('/tree', express.static('tree'));

if (config.ssl) {
	app = https.createServer(config.ssl, app)
}

app.listen(config.app.port, config.app.ip, function() {
	console.log('%s: Node server started on %s:%d ...', Date(Date.now()), config.app.ip, config.app.port);
});


