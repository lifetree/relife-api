var utils = require('../libs/utils');

module.exports = function (spec, config) {

	return function (req, res, next) {
		req.meta = utils.getMetaByUri(spec, req.url.replace(config.api.basePath, ''), req.method.toLowerCase());
		next();
	}

};
