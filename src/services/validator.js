var validator = require('../libs/validator');

/**
 * Сервис проверяет корректность входных данных
 * */
module.exports = function (definitions) {

	return function (req, res, next) {
		var method = req.method.toLowerCase(),
			data = req.body || req.parameters || {},
			errors;

		if (method === 'options') {
			return next();
		}

		if (req.url === '/api' || !/\/api.*/.test(req.url)) {
			return next();
		}

		if (!req.meta.schema) {
			return next();
		}

		errors = validator.validate(data, definitions[req.meta.schema]);

		if (errors && Object.keys(errors).length) {
			res.badRequest(errors);
			return;
		}

		return next();
	};

};
