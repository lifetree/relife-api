var base64 = require('base-64'),
	md5 = require('md5'),
	sessions = require('../../libs/sessions'),
	extend = require('extend');

/**
 * Метод проверяет базовую авторизацию пользователя
 * @param req
 * @param db
 * @param next
 */
module.exports = function (req, db, next) {
	var auth = req.headers.authorization,
		secret,
		data;

	if (!auth) {
		req.log('BasicAuth: request does not contain Authorization header');
		return next(false);
	}

	if (!/Basic\s/.test(auth)) {
		return next(false);
	}

	secret = auth.replace(/Basic (.+)/, '$1');
	data = base64.decode(secret).split(':');

	db.collection('users').findOne({email: data[0], pwd: md5(data[1])}, function (err, usr) {

		if (err) {
			return next(false);
		}

		if (!usr) {
			req.log('Auth: user not found', data[0], md5(data[1]));
			return next(false);
		}

		sessions.open(secret, usr, db, function (err, session) {

			if (err) {
				req.fatal('cant open new user session', err);
				return next(err);
			}

			req = extend(req, { session: session });
			next(true);
		});

	});

};
