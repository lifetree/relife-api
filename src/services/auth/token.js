var sessions = require('../../libs/sessions'),
	extend = require('extend');

/**
 * Метод проверяет авторизацию пользователя через токен
 * @param req
 * @param db
 * @param next
 */
module.exports = function (req, db, next) {
	var auth = req.headers.authorization,
		secret,
		data;

	if (!auth) {
		return next(false);
	}

	if (!/Token\s/.test(auth)) {
		return next(false);
	}

	secret = auth.replace(/Token token="(.+)"/, '$1');

	sessions.get(secret, db, function (err, data) {

		if (err) {
			req.fatal('cant get user session', err);
			return next(false);
		}

		if (!data) {
			return next(false);
		}

		req = extend(req, { session: data });
		next(true)
	});

};
