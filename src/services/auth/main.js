var mongodb = require('mongodb'),
	async = require('async');

var dbClient = require('../../libs/db');

function checkGrant(req, grant, type) {
	var session = req.session || {},
		regexp;

	if (grant.in === 'uri' && type == 'owner') {
		regexp = req.meta.expressRoute.replace(/^\//, '');
		regexp = '.*' + regexp + '.*';
		regexp = regexp.replace(':' + grant.value, session.id);
		regexp = regexp.replace(/\:[^\/]+/g, '.*');
	}

	return new RegExp(regexp).test(req.url);
}

module.exports = function (types) {

	types = types || {
		basicAuth: require('./basicAuth'),
		token: require('./token')
	};

	return dbClient.open(function (db, req, res, done) {

		if (req.url === '/api' || !/\/api.*/.test(req.url)) {
			return done();
		}

		if (!req.meta.security) {
			return done();
		}

		async.some(req.meta.security, function (rule, next) {
			types[rule](req, db, next);
		}, function (isAuth) {
			var grants = req.meta.grants,
				key;

			if (!isAuth) {
				res.headers({'WWW-Authenticate': 'Token'}).unauthorized({});
				return;
			}

			for (key in grants) {
				if (!checkGrant(req, grants[key], key)) {
					req.log('grant is incorrect', req.url, grants[key], key);
					res.forbidden(' ');
					return;
				}
			}

			done(null);
		});

	});

};
