(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("xhr2"));
	else if(typeof define === 'function' && define.amd)
		define("api", ["xhr2"], factory);
	else if(typeof exports === 'object')
		exports["api"] = factory(require("xhr2"));
	else
		root["api"] = factory(root["xhr2"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__) {
	return /******/ (function(modules) { // webpackBootstrap
		/******/ 	// The module cache
		/******/ 	var installedModules = {};

		/******/ 	// The require function
		/******/ 	function __webpack_require__(moduleId) {

			/******/ 		// Check if module is in cache
			/******/ 		if(installedModules[moduleId])
			/******/ 			return installedModules[moduleId].exports;

			/******/ 		// Create a new module (and put it into the cache)
			/******/ 		var module = installedModules[moduleId] = {
				/******/ 			exports: {},
				/******/ 			id: moduleId,
				/******/ 			loaded: false
				/******/ 		};

			/******/ 		// Execute the module function
			/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

			/******/ 		// Flag the module as loaded
			/******/ 		module.loaded = true;

			/******/ 		// Return the exports of the module
			/******/ 		return module.exports;
			/******/ 	}


		/******/ 	// expose the modules object (__webpack_modules__)
		/******/ 	__webpack_require__.m = modules;

		/******/ 	// expose the module cache
		/******/ 	__webpack_require__.c = installedModules;

		/******/ 	// __webpack_public_path__
		/******/ 	__webpack_require__.p = "/";

		/******/ 	// Load entry module and return exports
		/******/ 	return __webpack_require__(0);
		/******/ })
		/************************************************************************/
		/******/ ([
		/* 0 */
		/***/ function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_0__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_1__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_2__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_3__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_4__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_5__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_6__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_7__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_8__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/**
			 * Cross-Origin requests for IE 8—9
			 * Uses iframe and Cross Document Messaging
			 */
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_0__ = (function () {

				var processing = {};

				function guid() {
					return +new Date() + '' + Math.ceil(Math.random() * 10000);
				}

				function getCORURI(request, options) {
					// TODO: always ssl
					var uri = request.uri,
						host,
						scheme = '',
						cor = '',
						_;

					_ = uri.split('//');

					if (_.length > 1) {
						if (_[0].substr(_[0].length - 1) === ':') {
							scheme = _[0];
						}

						_ = _[1].split('/');
						host = _[0];
					}

					if (host) {
						cor = scheme + '//' + host;
					}

					cor += options.cor;

					return cor;
				}

				function getProxy(uri, callback) {
					if (!getProxy.mems) {
						getProxy.mems = {};
					}

					if (getProxy.mems[uri]) {
						callback(getProxy.mems[uri]);
						return;
					}

					var div = document.createElement('div'),
						id = 'i' + guid(),
						onload = 'f' + guid();

					div.style.width = div.style.height = '0px';
					div.style.visibility = div.style.overflow = 'hidden';
					div.innerHTML = '<iframe id="' + id + '" src="' + uri + '" style="" onload="' + onload + '()" />';

					window[onload] = function () {
						getProxy.mems[uri] = document.getElementById(id).contentWindow;
						callback(getProxy.mems[uri]);
					};

					document.body.appendChild(div);
				}

				function handle(e) {
					e = e || event;

					var message, xhr;

					try {
						message = JSON.parse(e.data);
					} catch (e) {
						return;
					}

					if (message.type !== 'cor' || !processing[message.id]) {

						return;
					}

					xhr = processing[message.id];
					processing[message.id] = undefined;
					delete processing[message.id];

					// Normalize IE 1223 error to 204 response
					if (message.status === 1223) {
						message.status = 204;
						message.statusText = 'No Content';
					}

					// Normalize IE errors to 0 response
					// https://msdn.microsoft.com/en-us/library/windows/desktop/aa385465(v=vs.85).aspx
					if (message.status > 12000) {
						message.status = 0;
					}

					// IE send status 2 when Windows/IE is set to "Work offline" and the resource is not in the offline cache.
					// https://msdn.microsoft.com/en-us/library/windows/desktop/aa384247(v=vs.85).aspx
					if (message.status === 2) {
						message.status = 0;
					}

					xhr.status = message.status;
					xhr.responseText = message.response;
					xhr.headers = message.headers;

					message.status ? xhr.onload() : xhr.onerror({});
				}

				function send(request, options, xhr) {
					var uri = getCORURI(request, options),
						id = guid();

					processing[id] = xhr;

					getProxy(uri, function (proxy) {
						proxy.postMessage(JSON.stringify({
							id: id,
							req: request
						}), '*');
					});
				}

				if (typeof window !== 'undefined') {
					if (window.addEventListener) {
						window.addEventListener('message', handle, false);
					} else if (window.attachEvent) {
						window.attachEvent('onmessage', handle);
					}
				}

				return function (options) {

					var request = { headers: {} };

					this.headers = {};
					this.onload = null;
					this.onerror = null;
					this.withCredentials = false;

					this.open = function (method, uri) {
						request.method = method;
						request.uri = uri;
					};

					this.setRequestHeader = function (name, value) {
						request.headers[name] = value;
					};

					this.getResponseHeader = function (name) {
						return this.headers[name.toLowerCase()];
					};

					this.send = function (body) {
						request.body = body;

						send(request, options, this);
					};
				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			var XMLHttpRequest;

			if (typeof window === 'undefined') {
				XMLHttpRequest = __webpack_require__(1);
			} else {
				XMLHttpRequest = window.XMLHttpRequest;
			}

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, __WEBPACK_LOCAL_MODULE_0__], __WEBPACK_LOCAL_MODULE_1__ = (function (require) {
				// TODO: async load for IE only
				var COR = __WEBPACK_LOCAL_MODULE_0__;

				var Request,
					buffer = [],
					ready = false;

				var media = {
					json: 'application/json'
				};

				if (typeof document === 'undefined' || 'withCredentials' in new XMLHttpRequest() && document.documentMode !== 10) {
					/*
					 because of bug in IE 10 there is no way
					 to recognize 401 response status code
					 for cross-origin requests
					 https://connect.microsoft.com/IE/feedback/details/785990
					 this actually means IE10 does't support CORS
					 */

					Request = XMLHttpRequest;
					ready = true;
				} else {
					// TODO: async with flush()
					Request = COR;
					ready = true;
				}

				function flush() {
					ready = true;

					buffer.forEach(function (req) {
						exports.request(req.options, req.callback);
					});

					buffer = undefined;
				}

				function defaults(options) {
					function def(name, value) {
						options[name] || (options[name] = value);
					}

					def('method', 'GET');
					def('headers', {});
					def('xhr', {});

					options.media || (options.media = media.json);
					options.xhr.cor || (options.xhr.cor = '/cor.html');
					options.xhr.timeout || (options.xhr.timeout = 30000);
				}

				function parse(str) {
					try {
						return JSON.parse(str);
					} catch (e) {
						return null;
					}
				}

				function stringify(obj) {
					try {
						return JSON.stringify(obj);
					} catch (e) {
						return '';
					}
				}

				function normalize(options) {
					var temp,
						headers = options.headers;

					// normalize headers
					for (var name in headers) {
						temp = headers[name];
						delete headers[name];

						headers[name.toLowerCase()] = temp;
					}
				}

				return {

					request: function (options, callback) {

						if (!ready) {
							buffer.push({ options: options, callback: callback });
							return;
						}

						defaults(options);
						normalize(options);

						var req = new Request(options.xhr);

						options.headers.accept || (options.headers.accept = options.media);

						if (options.body !== undefined) {
							options.headers['content-type'] = options.media;
						}

						req.open(options.method, options.uri);
						req.withCredentials = !!options.cookie;

						if (options.headers) {
							for (var name in options.headers) {
								req.setRequestHeader(name, options.headers[name]);
							}
						}

						req.onload = function () {
							var xx = Math.floor(req.status / 100),
								err = null;

							if (xx == 4 || xx == 5) {
								err = { status: req.status };
							}

							var res = req.responseText ? parse(req.responseText) : null;

							callback(err, res, req);
						};

						req.onerror = function (e) {
							e.status = 0;
							callback(e, null, req);
						};

						req.send(stringify(options.body));
					}
				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			/**
			 * Access Authentication Framework
			 * http://tools.ietf.org/html/rfc2617#section-1.2
			 */

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_2__ = (function () {
				"use strict";

				var firstTokenRegExp = /^.+?\b/,
					paramsRegExp = /(\w+)="([^"]+)"/g;

				function parse(str) {
					var context = this;

					// first token
					this.scheme = firstTokenRegExp.exec(str)[0];
					this.params = {};

					if (paramsRegExp.test(str)) {

						// all quoted parameters (unquoted not supported)
						str.replace(paramsRegExp, function (_, name, value) {
							context.params[name] = value;
						});
					} else {
						this.params = str.replace(firstTokenRegExp, '').trim();
					}
				}

				function stringify() {

					if (this.scheme === undefined) {
						return;
					}

					if (typeof this.params === 'string') {
						// Basic scheme doesnt fit Framework
						// http://www.rfc-editor.org/errata_search.php?rfc=2617&eid=1959
						return this.scheme + ' ' + this.params;
					}

					var list = [],
						value;

					for (var name in this.params) {
						value = this.params[name];

						if (value !== undefined) {
							list.push(name + '="' + value + '"');
						}
					}

					return this.scheme + (list.length ? ' ' + list.join(', ') : '');
				}

				return function (scheme, params) {

					if (params === undefined && scheme !== undefined) {
						parse.call(this, scheme);
					} else {
						this.scheme = scheme;
						this.params = params;
					}

					this.update = function (scheme, params) {

						var oldScheme = this.scheme;

						typeof scheme === 'object' ? params = scheme : this.scheme = scheme;

						if (params === undefined) {
							return this.toString();
						}

						if (typeof params === 'string') {
							this.params = params;
							return this.toString();
						}

						if (typeof this.params !== typeof params) {
							this.params = params;
						} else {

							if (scheme !== oldScheme) {
								this.params = {};
							}

							for (var name in params) {
								this.params[name] = params[name];
							}
						}

						return this.toString();
					};

					this.toString = function () {
						return stringify.call(this);
					};
				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));
			/**
			 * Base64 encoding/decoding
			 */

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_3__ = (function () {
				"use strict";

				var charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

				return {
					encode: function (input) {
						var output = [],
							c1,
							c2,
							c3,
							e1,
							e2,
							e3,
							e4,
							i = 0;

						input = unescape(encodeURIComponent(input));

						while (i < input.length) {

							c1 = input.charCodeAt(i++);
							c2 = input.charCodeAt(i++);
							c3 = input.charCodeAt(i++);

							e1 = c1 >> 2;
							e2 = (c1 & 3) << 4 | c2 >> 4;
							e3 = (c2 & 15) << 2 | c3 >> 6;
							e4 = c3 & 63;

							if (isNaN(c2)) {
								e3 = e4 = 64;
							} else if (isNaN(c3)) {
								e4 = 64;
							}

							output.push(charset.charAt(e1));
							output.push(charset.charAt(e2));
							output.push(charset.charAt(e3));
							output.push(charset.charAt(e4));
						}

						return output.join('');
					}
				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, __WEBPACK_LOCAL_MODULE_2__, __WEBPACK_LOCAL_MODULE_3__], __WEBPACK_LOCAL_MODULE_4__ = (function (require) {

				var AAF = __WEBPACK_LOCAL_MODULE_2__,
					base64 = __WEBPACK_LOCAL_MODULE_3__;

				return function () {

					var value = new AAF();

					var schemes = {

						'basic': function (login, password) {
							return authorize('Basic', base64.encode(login + ':' + password));
						},

						'token': function (token, realm) {
							return authorize('Token', { token: token, realm: realm });
						},

						'facebook': function (token) {
							return authorize('Facebook', { token: token });
						}

					};

					function authorize(scheme, params) {

						if (scheme === undefined) {
							return value.toString();
						}

						if (params === undefined) {
							// raw authentification header
							return value = new AAF(scheme);
						}

						return value.update(scheme, params);
					}

					for (var scheme in schemes) {
						authorize[scheme] = schemes[scheme];
					}

					authorize.realm = function (realm) {

						return realm === undefined ? value && value.params && value.params.realm // slicky code but it works (params can be a string)
							: value.update(value.scheme, { realm: realm });
					};

					this.authorize = authorize;

					this.unauthorize = function () {
						value = new AAF();
					};
				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_5__ = (function () {
				var uribraces = encodeURIComponent('[]');

				return {

					encode: function encode(obj) {
						var params = [];

						if (!(obj instanceof Object)) {
							return encodeURIComponent(obj);
						}

						for (var name in obj) {
							var value = obj[name];

							if (value instanceof Array) {
								value.forEach(function (item) {
									params.push(encodeURIComponent(name) + uribraces + '=' + encode(item));
								});
							} else {
								params.push(encodeURIComponent(name) + '=' + encodeURIComponent(value));
							}
						}

						return params.length ? '?' + params.sort().join('&') : '';
					}

				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			/**
			 * Thrown exception
			 */
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_6__ = (function () {
				"use strict";

				function Exception(err, res) {
					for (var name in res) {
						this[name] = res[name];
					}

					err && (this.status = err.status);
				}

				Exception.prototype = new Error();
				Exception.prototype.constructor = Exception;

				return Exception;
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, __WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_4__, __WEBPACK_LOCAL_MODULE_5__, __WEBPACK_LOCAL_MODULE_6__], __WEBPACK_LOCAL_MODULE_7__ = (function (require) {
				var transport = __WEBPACK_LOCAL_MODULE_1__,
					auth = __WEBPACK_LOCAL_MODULE_4__,
					urlencode = __WEBPACK_LOCAL_MODULE_5__,
					Exception = __WEBPACK_LOCAL_MODULE_6__;

				var placeholder = /^{(.+)}$/,
					safeMethods = { 'GET': true, 'HEAD': true, 'OPTIONS': true },
					synonyms;

				synonyms = {
					'PUT': ['save', 'replace'],
					'POST': ['add'],
					'DELETE': ['remove']
				};

				function result(value) {

					if (value.indexOf('=') === -1) {
						return value;
					}

					var parts = value.split('=');

					// TODO: remove primitive type object wrapper
					value = new String(parts[0]);
					value.variable = parts[1];

					return value;
				}

				function last(arr, except) {
					except || (except = 0);
					return typeof arr[arr.length - except - 1];
				}

				return function apic(desc, options) {
					options = options || {};

					var root = {},
						base = desc.base,
						variables = {},
						augments = {},
						language,
						lastResponse;

					var scheme = 'http:';

					options.synonyms = options.synonyms || synonyms;
					options.tokenHeader = options.tokenHeader || 'X-Security-Token';
					options.authorizationHeader = options.authorizationHeader || 'Authorization';

					if (typeof location !== 'undefined') {
						scheme = location.protocol; // current URI scheme
					}

					auth.apply(root);

					root.augment = function (name, value) {

						if (value === undefined) {
							return augments[name];
						}

						augments[name] = value;
					};

					root.localize = function (value) {

						if (value !== undefined) {
							language = value;
						}

						return language;
					};

					root.variable = function (name, value) {

						if (value !== undefined) {
							variables[name] = value;
						}

						return variables[name];
					};

					root.getLastResponse = function () {
						return lastResponse;
					};

					root.getBaseHost = function () {
						return base;
					};

					function method(verb, uri, res, params, secure) {
						var safe = safeMethods[verb] || false,
							protocol = (secure = secure || !safe) ? 'https:' : scheme;

						// do not ever use this
						if (options.__forceProtocol) {
							protocol = options.__forceProtocol;
						}

						return function () {
							var args = Array.prototype.slice.call(arguments),
								headers = last(args) === 'object' && last(args, 1) === 'function' ? args.pop() : {},
								callback = last(args) === 'function' || last(args) === 'undefined' ? args.pop() : undefined,
								representation = last(args) === 'object' ? args.pop() : undefined,
								query = last(args) === 'object' ? args.pop() : {},
								path = uri,
								_;

							if (safeMethods[verb]) {
								query = representation || {};
								representation = undefined;
							}

							/*
							 Check if all necessary arguments passed in arguments or have default variable value
							 */
							if (params.reduce(function (a, b) {
									return a - +(query[b] !== undefined || variables[b.variable] !== undefined);
								}, params.length) > args.length) {

								throw new Error([params.length, ' argument', params.length === 1 ? '' : 's', params.length ? ' (' + params.join(', ') + ')' : '', ' expected ', args.length, ' given', args.length ? ' (' + args.join(', ') + ')' : ''].join(''));
							}

							params.forEach(function (name, i) {

								if (!query[name]) {
									query[name] = args[i] === undefined ? name.variable && variables[name.variable] : args[i];
								}

								if (name.template) {
									var _ = '{' + name + '}',
										value = args[i] || query[name];

									path.indexOf(_) === -1 ? path += '/' + value : path = path.replace(_, value);

									delete query[name];
								}
							});

							// cleanup
							for (_ in query) {

								if (query[_] === undefined) {
									delete query[_];
								}
							}

							if (secure && root.authorize()) {
								headers[options.authorizationHeader] = root.authorize();
							}

							if (language) {

								if (safe) {
									headers['Accept-Language'] = language;
								} else if (representation) {
									headers['Content-Language'] = language;
								}
							}

							for (_ in augments) {

								if (augments[_] !== null) {
									headers[_] = augments[_];
								}
							}

							transport.request({
								uri: protocol + base + path + urlencode.encode(query),
								method: verb,
								headers: headers,
								body: representation,
								cookie: options.cookie
							}, function (err, body, xhr) {
								var token;

								function cb(err) {

									if (!callback) {
										return;
									}

									var args = [err, body];

									/**
									 * Pass xhr object only if explicitly expected
									 * otherwise some libraries using 'arguments' object
									 * can work incorrectly
									 */
									callback.length === 3 && args.push(xhr);
									callback.apply(this, args);
								}

								lastResponse = xhr;

								if (err) {

									if (err.status === 401) {
										root.unauthorize();
									}

									cb(new Exception(err, body));

									return;
								}

								res.variable && (variables[res.variable] = body && body[res]);

								if (secure) {
									token = xhr.getResponseHeader(options.tokenHeader);
									token && root.authorize.token(token, root.authorize.realm());
								}

								cb(null);
							});
						};
					}

					return function generate(map, uri, res) {
						var key, value, parts, secure, transparent, tree, _;

						res = res || {};

						for (key in map) {
							if (map.hasOwnProperty(key) && key !== '@') {
								value = map[key];

								transparent = key.match(placeholder);

								key = (parts = key.split('#')).shift();

								parts = parts.map(function (name) {
									var template, variable, match;

									if (name.charAt(0) === '/') {
										template = true;
										name = name.substr(1);
									}

									if (match = name.match(placeholder)) {
										_ = match[1].split('=');

										name = _[0];
										variable = _[1];
									}

									// monkey code
									// TODO: remove primitive type object wrapper
									name = new String(name);
									name.template = template;
									name.variable = variable;

									return name;
								});

								if (key.charAt(key.length - 1) === '*') secure = !!(key = key.substring(0, key.length - 1));

								if (key.charAt(0) !== ':') {
									_ = key.toLowerCase();

									tree = typeof value === 'string' ? method(key, uri || '/', result(value), parts, secure) : generate(value, (uri || '') + '/' + (value['@'] || key), transparent && res);

									transparent || (res[_] = tree);

									options.synonyms[key] && options.synonyms[key].forEach(function (alias) {
										res[alias] || (res[alias] = res[_]);
									});
								} else res[key.substr(1)] = res[value.toLowerCase()];
							}
						}

						return res;
					}(desc.resources, undefined, root);
				};
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_7__], __WEBPACK_LOCAL_MODULE_8__ = (function (main) {
				return main;
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));

			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, __WEBPACK_LOCAL_MODULE_8__, __webpack_require__(2)], __WEBPACK_AMD_DEFINE_RESULT__ = function (require) {
				var apic = __WEBPACK_LOCAL_MODULE_8__,
					desc = __webpack_require__(2),
					api = apic(desc);

				return api;
			}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

			/***/ },
		/* 1 */
		/***/ function(module, exports) {

			module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

			/***/ },
		/* 2 */
		/***/ function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function () {
				return "{descriptor}";
			}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

			/***/ }
		/******/ ])
});
;
