var Model = require('./Model'),
	Task = require('./Task'),
	extend = require('deep-extend'),
	validator = require('../libs/validator'),
	async = require('async');

var schemes = {
	habit: require('../../api/scheme/Habit'),
	goal: require('../../api/scheme/Goal'),
	milestone: require('../../api/scheme/Milestone')
};

var COLLECTION_NAME = 'milestones';

/**
 * Конструктор нового мейлстоуна
 * @param {object} data - данные
 * @constructor
 */
var Milestone = function (data) {
	extend(this, Model.prototype);
	extend(this, data);

	this.coreId = +data.coreId;
	this.ownerId = +data.ownerId;
};

/**
 * Метод проверяет данные модели
 * @returns {boolean|object} - если модель заполненна не правильно, вернет список ошибок
 */
Milestone.prototype.validate = function () {
	var errors, thingErrors;

	// кастомная валидация
	errors = validator.validate(this, schemes.milestone);
	thingErrors = validator.validate(this.thing || {}, schemes[this.type]);

	if (thingErrors && Object.keys(thingErrors).length) {
		errors.thing = thingErrors;
	}

	if (errors && Object.keys(errors).length) {
		return errors;
	}

	return false;
};

/**
 * Метод сохраняет новую модуль в базе
 * @param db - клиент базы
 * @param done - функция обратного вызова
 */
Milestone.prototype.save = function (db, done) {
	var err;

	if (err = this.validate()) {
		return done(err);
	}

	async.waterfall([

		// находим ценность привязанную к этой задачи
		function (next) {

			db.collection('cores').findOne({
				id: this.coreId,
				ownerId: this.ownerId
			}, next);

		}.bind(this),

		// находим общее количество этих задач
		function (core, next) {

			if (!core) {
				return next({coreId: 'not_exist'});
			}

			if (this.id) {
				return next(null, this.id);
			}

			db.collection(COLLECTION_NAME).count(next);
		}.bind(this),

		// создаем новый мейлстоун
		function (count, next) {

			if (!this.id) {
				this.createDate = Date.now();
				this.id = count + 1;
			}

			db.collection(COLLECTION_NAME).update({id: this.id}, this.toJSON(), {upsert: true, multi: false}, next);
		}.bind(this)

	], function (err) {

		if (err) {
			return done(err);
		}

		done(null);
	});

};

Milestone._getQuery = function (qs) {
	var query = {};

	if (qs.type && ['habit', 'goal'].indexOf(qs.type) !== -1) {
		query.type = qs.type;
	}

	// включать или не включать удаленные
	if (qs.deleted === 'true') {
		query.deletedDate = { $exists: true };
	} else {
		query.deletedDate = { $exists: false };
	}

	// показать только заврешенные
	if (qs.completed === 'true') {
		query.completedDate = { $exists: true };
	} else {
		query.completedDate = { $exists: false };
	}

	if (qs.parent) {
		query.parentId = +qs.parent;
	}

	if (qs.id) {
		query.id = +qs.id;
	}

	if (qs.cores) {
		query.coreId = { $in: qs.cores.split(',').map(function(i) { return +i }) };
	}

	if (qs.ownerId) {
		query.ownerId = +qs.ownerId;
	}

	return query;
};


/**
 * Метод получает список мейлстоунов
 * @param {object} qs - запрос
 * @param db - клиент базы
 * @param done - функция обратного вызова
 */
Milestone.find = function (qs, db, done) {

	async.parallel({

		milestones: function (next) {
			Model.find.call(Milestone, db.collection(COLLECTION_NAME), Milestone._getQuery(qs), next);
		},

		tasks: function (next) {
			Task.find(db, qs, next);
		}
	}, function (err, res) {
		var milestones;

		if (err) {
			return done(err);
		}

		res.tasks = res.tasks || [];
		res.milestones = res.milestones || [];

		milestones = res.milestones.map(function (ms) {

			ms.tasks = res.tasks.filter(function (task) {
				return task.milestoneId === ms.id;
			});

			return ms;
		});

		done(null, milestones);
	})


};

Milestone.findOne = function (db, qs, done) {
	var query = {};

	if (qs.id) {
		query.id = +qs.id;
	}

	if (qs.userid) {
		query.ownerId = +qs.userid;
	}

	Model.findOne.call(Milestone, db.collection(COLLECTION_NAME), query, done);
};

module.exports = Milestone;
