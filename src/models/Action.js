var extend = require('deep-extend'),
	Model = require('./Model');

var MS_IN_DAY = 86400000,
	MS_IN_WEEK = 604800000;

var DEFAULT_COUNT = 21;

var WEEKDAY_IDS = {
	"sun": 0,
	"mon": 1,
	"tue": 2,
	"wed": 3,
	"thu": 4,
	"fri": 5,
	"sat": 6
};

Date.prototype.setDay = function(dayOfWeek) {
	this.setDate(this.getDate() - this.getDay() + dayOfWeek);
};

var strategies = {

	/**
	 * Ставим задачу на следующий день
	 * @param prev - дата предыдущей задачи
	 * @returns {*}
	 */
	daily: function (prev) {
		return prev + MS_IN_DAY;
	},

	/**
	 * Ставим задачу на следующую неделю
	 * @param prev - дата предыдущей задачи
	 * @returns {*}
	 */
	weekly: function (prev) {
		return prev + MS_IN_WEEK;
	},

	/**
	 * Ставим задачу на следующий год
	 * @param prev - дата предыдущей задачи
	 * @returns {*}
	 */
	yearly: function (prev) {
		var dt = new Date(prev);
		dt.setYear(dt.getYear() + 1901);
		return dt.getTime();
	},

	/**
	 * Ставим задачу на дни неделиы
	 * @param prev - дата предыдущей задачи
	 * @param {object} duration - повторяемость задачи
	 * @returns {*}
	 */
	weekdays: function (prev, duration) {

		return duration.weekdays.map(function (weekday) {
			var date = new Date(prev);

			weekday = WEEKDAY_IDS[weekday];

			if (weekday > date.getDay()) {
				date.setDay(weekday);
			} else {
				// если на этой неделе день недели прошел, ставим на следующую
				date.setDay(weekday + 7);
			}

			prev = date.getTime();
			return prev;
		});

	},

	/**
	 * Ставим задачу на интервал
	 * @param prev - дата предыдущей задачи
	 * @param {object} duration - повторяемость задачи
	 * @returns {*}
	 */
	interval: function (prev, duration) {
		return prev + duration.interval;
	}

};

function Action(start, completed, failed) {
	extend(this, Model.prototype);

	this.start = start;

	if (typeof completed === 'boolean') {
		this.completed = completed;
	}

	if (typeof failed === 'boolean') {
		this.failed = failed;
	}

}

/**
 * Метод генерирует коллекцию действий по заданному интервалу
 * @param task - модель таска, для которого создаются действия
 * @param req - объекта запроса, используется для журналирования
 */
Action.generate = function (task, req) {
	var dates = [],
		start = task.duration.startDate || Date.now(),
		count = task.duration.count || DEFAULT_COUNT;

	if (!strategies[task.duration.strategy]) {
		req.log('unknown strategy', task.duration.strategy);
		return false;
	}

	task.actions = task.actions || [];

	count = count - task.actions.length;
	req.log('actions use strategy', task.duration);

	while (count > 0) {
		start = strategies[task.duration.strategy](start, task.duration);

		if (Array.isArray(start)) {
			dates = dates.concat(start);
			count = count - start.length;
			start = start[start.length - 1];
		} else {
			dates.push(start);
			count--;
		}

	}

	// удаляем лишний хвост
	if (count < 0) {

		while (count !== 0) {
			dates.pop();
			count ++;
		}

	}

	dates = dates.map(function (s) {

		if (isNaN(s)) {
			console.log('sfaafsafsdafdsafsdfsdafsadsadf', '\n', '\n');
		}

		return new Action(s);
	});

	task.actions = task.actions.concat(dates);
	return true;
};

module.exports = Action;
