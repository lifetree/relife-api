var clone = require('clone');

function Model(){}

/**
 * Метод ищет коллекцию моделей в хранилище
 * @param {collection} collection - коллекция
 * @param {object} query - запрос
 * @param {function} done - функция обратного вызова
 */
Model.find = function (collection,  query, done) {
	var result = [],
		range = {},
		cursor;

	if (query.skip) {
		range.skip = +query.skip;
		delete query.skip;
	}

	if (query.limit) {
		range.limit = +query.limit;
		delete query.limit;
	}

	cursor = collection.find(query, { _id: false});

	if (range.limit) {
		cursor.limit(range.limit);
	}

	if (range.skip) {
		cursor.skip(range.skip);
	}

	cursor.each(function (err, item) {

		if (err) {
			return done(err);
		}

		if (!item) {
			return done(null, result);
		}

		result.push(new this(item));
	}.bind(this));

};

/**
 * Метод получает один объект
 * @param {collection} collection - коллекция
 * @param {object} query - запрос
 * @param {function} done - функция обратного вызова
 */
Model.findOne = function (collection,  query, done) {
	query = query || {};
	query.limit = 1;

	Model.find.call(this, collection, query, function (err, res) {

		if (err) {
			return done(err);
		}

		done(null, res[0]);
	});

};


Model.prototype.toJSON = function () {
	var obj = clone(this, false);

	delete obj._data;

	return obj;
};

module.exports = Model;
