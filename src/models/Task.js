var Model = require('./Model'),
	Action = require('./Action'),
	extend = require('deep-extend'),
	validator = require('../libs/validator'),
	async = require('async');

var COLLECTION_NAME = 'tasks';

var schemes = {
	task: require('../../api/scheme/Task'),
	action: require('../../api/scheme/Action')
};

function Task(data) {
	extend(this, Model.prototype);
	extend(this, data);

	this._data = data; // данные с которыми была модель создана

	this.ownerId = +data.ownerId;
	this.milestoneId = +data.milestoneId;
}

/**
 * Метод валидирует модель таска и в случае ошибки возвращает их
 * @return {null|object} - если есть не валидные поля, то возвращает их список
 */
Task.prototype.validate = function () {
	var err = validator.validate(this, schemes.task);

	if (err && Object.keys(err).length) {
		return err;
	}

	return false;
};

/**
 * Метод обновляет или создает новый таск в системе
 * @param {db} db - клиент базы данных
 * @param {req} req - объект запроса, для которого генерируется Task
 * @param {object} newData - новые данные
 * @param {function} done - колбек
 */
Task.prototype.save = function (db, req, done) {
	var err = this.validate();

	if (err) {
		return done(err);
	}


	async.waterfall([

		function (next) {

			if (this.id) {
				return next(null);
			}

			db.collection(COLLECTION_NAME).count(next);
		}.bind(this),

		function (count, next) {

			if (typeof count == 'function') {
				next = count;
				count = null;
			}

			if (!this.id) {
				this.id = count + 1;
				this.createdDate = Date.now();

				req.log('generate actions for new Task');

				if (!Action.generate(this, req)) {
					return next(new Error({actions: 'error'}));
				}

			}

			db.collection(COLLECTION_NAME).update({id: this.id}, this.toJSON(), {upsert: true, multi: false}, next);
		}.bind(this)

	], function (err) {

		if (err) {
			return done(err);
		}

		done(null);
	}.bind(this));

};

/**
 * Метод начисляет пеналти на провал дейсвтия
 * @param {Request} req - объект запроса, нужен для журналирования
 */
Task.prototype.penalty = function (req) {
	var actions, i, n, penalty;

	actions = this.actions.sort(function (a, b) {
		return a.start - b.start;
	});

	for (i = 0; i < actions.length; i++) {

		if (!actions[i].failed) {
			break;
		}

	}

	n = i + 1;
	penalty = (1 + n)/2 * n; // арифметическая прогрессия
	req.log('accrued penalty', penalty);

	this.duration.count = actions.length + penalty;
	Action.generate(this, req);
};

/**
 * Продлевает таск (генерирует новый action)
 * @param  {Request} req - объект запроса, нужен для журналирования
 */
Task.prototype.prolong = function (req) {
	var actions = this.actions || [];

	req.log('prolong task');
	this.duration.count = actions.length + 1;
	this.duration.startDate = actions[actions.length - 1].start;
	Action.generate(this, req);
};


Task._getQuery = function (qs) {
	var query = {};

	// включать или не включать удаленные
	if (qs.deleted === 'true') {
		query.deletedDate = { $exists: true };
	} else {
		query.deletedDate = { $exists: false };
	}

	// показать только заврешенные
	if (qs.completed === 'true') {
		query.completedDate = { $exists: true };
	} else {
		query.completedDate = { $exists: false };
	}

	if (qs.mid) {
		query.milestoneId = +qs.mid
	}

	if (qs.userid) {
		query.ownerId = +qs.userid;
	}

	if (qs.id) {
		query.id = +qs.id;
	}

	return query;
};

/**
 * Метод ищет таски
 * @param {db} db - клиент бд
 * @param {object} qs - объект запроса
 * @param {function} done - фунция обратного вызова
 */
Task.find = function (db, qs, done) {
	Model.find.call(Task, db.collection(COLLECTION_NAME), Task._getQuery(qs), done);
};


/**
 * Метод удалеяет все таски привязанные к майлстоуну
 * @param {db} db - клиент бд
 * @param {number} mid - идентификатор мейлстоуна
 * @param {function} done - фунция обратного вызова
 */
Task.removeByMid = function (db, mid, done) {

	db.collection(COLLECTION_NAME).remove({
		milestoneId: +mid
	}, done);

};


/**
 * Метод находит один объект таска
 * @param db
 * @param qs
 * @param done
 */
Task.findOne = function (db, qs, done) {
	var query = {};

	if (qs.id) {
		query.id = +qs.id;
	}

	if (qs.mid) {
		query.milestoneId = +qs.mid;
	}

	if (qs.userid) {
		query.ownerId = +qs.userid;
	}

	Model.findOne.call(Task, db.collection(COLLECTION_NAME), query, done);
};

module.exports = Task;
