var async = require('async'),
	dbClient = require('../libs/db');

module.exports = function () {

	/**
	 * Метод проверяет наличие пользователя
	 * и если такого нет, то создает новый
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getUser(db, req, res) {
		var params = req.params || {};

		db.collection('users').find({
			id: +params.userid
		}, { _id: false, pwd: false }).toArray(function (err, user) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			if (!user) {
				req.log('Controllers.User: user not found', params);
				res.notFound().text(' ');
			} else {
				res.json(user[0]);
			}

		});

	}

	function patchUser(db, req, res) {
		var params = req.params || {},
			body = req.body || {};


		db.collection('users').update({
			id: +params.userid
		}, {
			$set: body
		}, function (err) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			res.json({});
		});

	}

	return {
		get: dbClient.open(getUser),
		patch: dbClient.open(patchUser)
	};

};
