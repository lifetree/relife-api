module.exports = function (spec) {
	var env = require('../libs/env'),
		api = require('../../api/api');

	var spec = api.getSwaggerSpec(env.getConfig());

	/**
	 * Модуль собирает спецификацию для swager.io
	 * @param {Request} req
	 * @param {Response} res
	 */
	return function (req, res) {
		res.json(spec);
	};

};
