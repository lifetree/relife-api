var async = require('async'),
	dbClient = require('../libs/db'),
	tasks = require('../libs/tasks');

var Milestone = require('../models/Milestone'),
	Task = require('../models/Task');

module.exports = function () {

	/**
	 * Метод получает список
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function list(db, req, res) {
		var qs = req.query || {},
			params = req.params || {};

		req.log('get milestones list', params, qs);
		db.ownerId = +params.userid;

		Milestone.find(qs, db, function (err, milestones) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				req.fatal('get milestones list error', err);
				res.json(err);
				return;
			}

			res.json(milestones);
		});

	}


	/**
	 * Метод создает новый milestone
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function create(db, req, res) {
		var params = req.params || {},
			body = req.body || {},
			model;

		model = new Milestone(body);
		model.ownerId = +params.userid;

		model.save(db, function (err) {

			if (err && err instanceof Error) {
				req.fatal('fatal error', err);
				res.error().json(err);
				return;
			}

			if (err) {
				req.log('incorrect data from client', err);
				res.badRequest().json(err);
				return;
			}

			req.log('milestones created', err);
			res.json(model.toJSON());
		});

	}

	return {
		list: dbClient.open(list),
		create: dbClient.open(create)
	};

};
