var Task = require('../models/Task'),
	async = require('async'),
	extend = require('deep-extend'),
	tasks = require('../libs/tasks'),
	dbClient = require('../libs/db');

module.exports = function () {

	function sortActions(a, b) {
		return a.start - b.start;
	}

	/**
	 * Получение списка цели
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getList(db, req, res) {
		var params = req.params || {};

		req.log('get tasks list', params);

		Task.find(db, params, function (err, tasks) {

			if (err && err instanceof Error) {
				req.fatal('server error', err);
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			res.json(tasks);
		});

	}

	/**
	 * Получение таска
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getOne(db, req, res) {
		var params = req.params || {};

		req.log('get task', params);

		Task.findOne(db, {
			ownerId: +params.userid,
			milestoneId: +params.mid,
			id: +params.taskid
		}, function (err, task) {

			if (err) {
				req.fatal('serve error', err);
				res.error.json(err);
				return;
			}

			if (!task) {
				req.log('not found', params);
				res.notFound().send('');
				return;
			}

			res.json(task);
		});

	}

	/**
	 * Изменение таска
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function patch(db, req, res) {
		var params = req.params || {},
			body = req.body || {};

		var query = {
			id: +params.taskid,
			milestoneId: +params.mid,
			ownerId: +params.userid
		};

		req.log('patch task', params, body);

		Task.findOne(db, query, function (err, model) {

			if (err) {
				req.fatal('server error', err);
				res.error().json(err);
				return;
			}

			if (!model) {
				req.log('not found', params);
				res.notFound().send('');
				return;
			}

			// если в запросе есть зафейленые действия
			if (body.actions && body.actions.length) {
				model.actions = model.actions.sort(sortActions);
				body.actions = body.actions.sort(sortActions);

				body.actions.forEach(function (action, i) {
					extend(model.actions[i], action);

					if (action.failed && model.penalized) {
						console.log('!!!!', model.penalized);

						req.log('task\'s changes have failed action', action);
						model.penalty(req);
					}

					if (action.completed && model.recurrent) {
						req.log('changes of recurrent task have completed action', action);
						model.prolong(req);
					}

				});

				delete body.actions;
			}

			model = extend(model, body);
			err = model.validate();

			if (err) {
				req.log('incorrect data', err);
				return res.badRequest().json(err);
			}

			req.log('save task', model.id);

			model.save(db, req, function (err) {

				if (err) {
					req.fatal('server error', err);
					res.error().json(err);
					return;
				}

				res.json({});
			});

		});

	}


	/**
	 * Создание таска
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function create(db, req, res) {
		var params = req.params || {},
			body = req.body || {},
			model;

		body.milestoneId = +params.mid;
		body.ownerId = +params.userid;

		req.log('create new task for', params);
		model = new Task(body);

		model.save(db, req, function (err) {

			if (err && err instanceof Error) {
				req.fatal('fatal error', err);
				res.error().json(err);
				return;
			}

			if (err) {
				req.log('incorrect data from client', err);
				res.badRequest().json(err);
				return;
			}

			req.log('task created', err);
			res.json(model.toJSON());
		});

	}

	return {
		list: dbClient.open(getList),
		one: dbClient.open(getOne),
		patch: dbClient.open(patch),
		create: dbClient.open(create)
	};

};
