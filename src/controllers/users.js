var async = require('async'),
	md5 = require('md5'),
	dbClient = require('../libs/db');

module.exports = function () {
	var COLLECTION_NAME = 'users';

	/**
	 * Метод проверяет наличие пользователя
	 * и если такого нет, то создает новый
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 */
	function create(db, req, res, done) {
		var user = {};

		async.waterfall([

			function (next) {
				req.log('check user by email, before create');

				db.collection(COLLECTION_NAME).findOne({
					email: req.body.email
				}, next);

			},

			function (user, next) {

				if (user) {
					req.log('user exist');
					res.badRequest({ email: 'exist' });
					return next(true);
				}

				db.collection(COLLECTION_NAME).count(next);
			},

			function (id, next) {
				user = req.body;

				user.id = id + 1;
				user.pwd = md5(user.pwd);

				req.log('create new user', user);
				db.collection(COLLECTION_NAME).insert(user, next);
			}

		], function (err) {

			if (err && err instanceof Error) {
				req.log('fatal error', err);
				res.error();
			}

			if (err) {
				req.log('user input error', err);
				res.json(err);
				return;
			}

			delete user._id;
			delete user.pwd;

			res.json(user);
		});

	}

	return {
		create: dbClient.open(create)
	};

};
