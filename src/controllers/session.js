var sessions = require('../libs/sessions');

module.exports = function () {

	/**
	 * Метод открывает новую сессию
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 */
	function getSession(req, res) {
		res.json(req.session);
	}

	return {
		get: getSession
	};

};
