module.exports = function (spec) {
	var fs = require('fs'),
		path = require('path'),
		url = require('url'),
		desc = require('../libs/descriptor'),
		api = require('../../api/api'),
		env = require('../libs/env');

	var AMD_TEMPLATE = 'define(function () { return $desc });';

	var spec = api.getSwaggerSpec(env.getConfig()),
		tmpl = fs.readFileSync('./src/tmpls/client.js').toString(),
		result;

	function getDesc(req) {
		var host = '//' +  req.headers.host + spec.basePath;

		if (!result) {
			req.log('generate descriptor for ', host);

			result = JSON.stringify({
				resources: desc.generate(spec.paths, spec.variables),
				base: host
			});

		}

		return result;
	}

	return {
		/**
		 * Модуль собирает спецификацию для swager.io
		 * @param {Request} req
		 * @param {Response} res
		 */
		descriptor: function (req, res) {
			res.javascript(AMD_TEMPLATE.replace('$desc', getDesc(req)));
		},

		client: function (req, res) {
			res.javascript(tmpl.replace('"{descriptor}"', getDesc(req)));
		}

	};

};
