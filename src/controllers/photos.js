var async = require('async'),
	fs = require('fs'),
	request = require('request'),
	dbClient = require('../libs/db'),
	cloudinary = require('cloudinary');

cloudinary.config({
	cloud_name: 'dgjc7xkao',
	api_key: '231386693212659',
	api_secret: '5MSLrPIJ90uH_obnBszn7xeXeaE'
});

var current = 0,
	max = 5;

var COLLECTION_NAME = 'photos';

module.exports = function () {

	/**
	 * Метод загружает новую фотографию в пользовательский профил
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function upload(db, req, res) {
		var params = req.params || {},
			files,
			tmp;

		if (current > max) {
			current = 0;
		} else {
			current++;
		}

		tmp = './tmp/' + current + '.png';
		req.log('upload user photo', params);
		files = Object.keys(req.files);

		if(!files.length) {
			req.log('no file in request body');
			res.badRequest().json({photo: 'required'});
			return;
		}

		async.series({

			fs: function (next) {
				fs.writeFile(tmp, req.files[files[0]].data, next);
			},

			photo: function (next) {
				var start = Date.now();

				req.log('send photo from tmp to cloudinary', tmp);

				cloudinary.uploader.upload(tmp, function (result) {
					var loaded = Date.now();

					if (!result.url) {
						return next(result);
					}

					if (loaded - start > 10000) {
						req.log('cloudinary to long upload photo', loaded - start);
					}

					next(null, {uri: result.url, publicId: result.public_id});
				});

			},

			id: function (next) {
				db.collection(COLLECTION_NAME).count(next);
			}

		}, function (err, data) {

			if (err) {
				req.fatal('upload error', err);
				return res.status(500).send(err);
			}

			data.photo.id = ++data.id;
			data.photo.ownerId = +params.userid;
			req.log('photo uploaded', data);

			db.collection(COLLECTION_NAME).insert(data.photo, function (err) {

				if (err) {
					req.fatal('error while loading the photo', err);
					return res.status(500).send(err);
				}

				res.json({
					id: data.photo.id
				});

			});
		});

	}


	/**
	 * Получение списка фотографий
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getList(db, req, res) {
		var params = req.params || {};

		async.waterfall([

			function (next) {
				req.log('get list from', COLLECTION_NAME);

				db.collection(COLLECTION_NAME).find({
					ownerId: +params.userid
				}, { _id: false, uri: false }).toArray(next);

			}

		], function (err, photos) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			res.json(photos.map(function (photo) {
				return photo.id;
			}));

		});

	}

	/**
	 * Метод получает фотографию пользователя
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getOne(db, req, res) {
		var params = req.params || {};

		async.waterfall([

			function (next) {
				req.log('get list from', COLLECTION_NAME, params);

				db.collection(COLLECTION_NAME).find({
					ownerId: +params.userid,
					id: +params.photo
				}, { _id: false }).toArray(next);

			}

		], function (err, photo) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			if (!photo || !photo[0]) {
				req.log('photo not found', params);
				res.notFound().send(' ');
				return;
			}

			req.pipe(request.get(photo[0].uri)).pipe(res);
		});

	}

	/**
	 * Метод удаляет фотографию пользователя
	 */
	function remove(db, req, res) {
		var params = req.params || {};

		var query = {
			id: +params.photo,
			ownerId: +params.userid
		};

		req.log('delete user photo', params);

		db.collection(COLLECTION_NAME).find(query).toArray(function (err, photo) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			if (!photo || !photo[0]) {
				req.log('photo not found', params);
				res.notFound().send(' ');
				return;
			}

			cloudinary.api.delete_resources(photo[0].publicId, function (result) {

				if (!result.deleted || !result.deleted[photo[0].publicId]) {
					req.fatal('photo delete error', result);
					return res.error().json(result);
				}

				req.log('user photo deleted from cloudinary', params);

				db.collection(COLLECTION_NAME).remove(query, function (err) {

					if (err) {
						req.fatal('photo delete error', result);
						return res.error().json(err);
					}

					req.log('user photo deleted', params);
					res.json({});
				})

			});

		})

	}

	return {
		upload: dbClient.open(upload),
		list: dbClient.open(getList),
		get: dbClient.open(getOne),
		remove: dbClient.open(remove)
	};

};
