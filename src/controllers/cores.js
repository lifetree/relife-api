var async = require('async'),
	dbClient = require('../libs/db');

var COLLECTION_NAME = 'cores';

module.exports = function () {

	/**
	 * Получение списка цели
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getList(db, req, res) {
		var params = req.params || {};

		async.waterfall([

			function (next) {
				req.log('get list from', COLLECTION_NAME);

				db.collection(COLLECTION_NAME).find({
					ownerId: +params.userid
				}, { _id: false }).toArray(next);

			}

		], function (err, cores) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			res.json(cores);
		});

	}

	/**
	 * Метод получает одну цель
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getOne(db, req, res) {
		var params = req.params || {};

		req.log('get core ', params.coreid);

		db.collection(COLLECTION_NAME).findOne({
			ownerId: +params.userid,
			id: +params.coreid
		}, { _id: false }, function (err, core) {

			if (err) {
				req.fatal('get core error', err);
				return res.json(err);
			}

			if (!core) {
				req.log('core not found', params);
				res.notFound().json({});
			} else {
				res.json(core);
			}

		});

	}


	/**
	 * Метод обновляет ресурс цели
	 * @param db
	 * @param req
	 * @param res
	 */
	function patch(db, req, res) {
		var params = req.params || {},
			body = req.body || {};

		req.log('patch core', params.coreid);

		db.collection(COLLECTION_NAME).update({
			ownerId: +params.userid,
			id: +params.coreid
		}, {
			$set: body
		}, function (err) {

			if (err) {
				res.json(err);
				return;
			}

			res.json({});
		});

	}


	/**
	 * Метод удаляет ценность пользователя
	 * @param db
	 * @param req
	 * @param res
	 */
	function remove(db, req, res) {
		var params = req.params || {};

		req.log('remove core', params);

		db.collection(COLLECTION_NAME).remove({
			ownerId: +params.userid,
			id: +params.coreid
		}, function (err, info) {

			if (err) {
				res.json(err);
				return;
			}

			req.log('core removed', info.result);
			res.json({});
		});

	}

	/**
	 * Метод проверяет наличие пользовательской цели
	 * и если такой нет, то создает новую
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 */
	function create(db, req, res) {
		var core = req.body || {},
			params = req.params || {};

		async.waterfall([

			function (next) {

				db.collection(COLLECTION_NAME).findOne({
					ownerId: +params.userid,
					name: core.name
				}, next);

			},

			function (core, next) {

				if (core) {
					res.badRequest({ name: 'exist' });
					return next(true);
				}

				db.collection(COLLECTION_NAME).count(next);
			},

			function (id, next) {
				core.id = id + 1;
				core.ownerId = +params.userid;

				db.collection(COLLECTION_NAME).insert(core, next);
			}

		], function (err) {

			if (err && err instanceof Error) {
				res.error();
			}

			if (err) {
				return;
			}

			delete core._id;
			delete core.pwd;

			res.json(core);
		});

	}

	return {
		one: dbClient.open(getOne),
		list: dbClient.open(getList),
		create: dbClient.open(create),
		patch: dbClient.open(patch),
		remove: dbClient.open(remove)
	};

};
