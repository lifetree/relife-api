var async = require('async'),
	extend = require('deep-extend'),
	tasks = require('../libs/tasks'),
	dbClient = require('../libs/db');

module.exports = function () {

	function _sortActions(a, b) {
		return a.start - b.start;
	}

	function _findFirst(a, pivot) {

		for (var i = 0; i < a.length; i ++) {

			if (a[i].start >= pivot) {
				return a[i].start;
			}

		}

		return -1;
	}

	function _getQuery(userid, qs) {
		var query = {}, range = {};

		query.ownerId = +userid;

		if (qs.milestones) {
			query.milestoneId = { $in: qs.milestones.split(',').map(function(i) { return +i }) };
		}

		range['$gte'] = qs.start;
		qs.end && (range['$lte'] = +qs.end);

		query.actions = {
			'$elemMatch': {
				start: range
			}
		};

		if (qs.failed) {
			query.actions['$elemMatch'].failed = true;
		}

		if (qs.completed) {
			query.actions['$elemMatch'].completed = true;
		}


		console.log(JSON.stringify(query));
		return query;
	}


	/**
	 * Получение списка цели
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getList(db, req, res) {
		var params = req.params || {},
			query = req.query || {},
			collection = db.collection('tasks');

		req.log('generate schedule reports', params, query);

		if (query.hasOwnProperty('start')) {
			query.start = +query.start;
		} else {
			query.start = Date.now();
		}

		collection.find(_getQuery(params.userid, query), {_id: false}, {
			limit: +params.limit || 30,
			skip: +params.skip || 0
		}).toArray(function (err, tasks) {

			if (err && err instanceof Error) {
				req.fatal('server error', err);
				res.error();
			}

			if (err) {
				res.json(err);
				return;
			}

			req.log('sort by pivot', query.start);

			tasks = tasks.map(function (a) {
				a.actions = a.actions.sort(_sortActions);
				return a;
			}).sort(function (a, b) {
				return _findFirst(a.actions, query.start) - _findFirst(b.actions, query.start);
			});

			res.json(tasks);
		});


	}


	return {
		list: dbClient.open(getList)
	};

};
