var async = require('async'),
	extend = require('deep-extend'),
	Task = require('../models/Task'),
	validator = require('../libs/validator'),
	dbClient = require('../libs/db');

var schemes = {
	habit: require('../../api/scheme/Habit'),
	goal: require('../../api/scheme/Goal'),
	milestone: require('../../api/scheme/Milestone')
};

var COLLECTION_NAME = 'milestones';

var Milestone = require('../models/Milestone');

module.exports = function () {

	/**
	 * Метод создает новый milestone
	 * @param {DBClient} db
	 * @param {Request} req
	 * @param {Response} res
	 * @param {Function} done
	 */
	function getOne(db, req, res) {
		var params = req.params || {};

		req.log('get user milestone', params);

		Milestone.findOne(db, {
			id: +params.mid,
			ownerId: +params.userid
		}, function (err, milestone) {

			if (err) {
				req.log('error', err);
				res.json(err);
				return;
			}

			if (!milestone) {
				req.log('milestone not found', params);
				res.notFound().send(' ');
			} else {
				res.json(milestone);
			}

		});

	}

	/**
	 * Метод обновляет данные о пользовательском мейлстоуне
	 * @param db
	 * @param req
	 * @param res
	 */
	function patch(db, req, res) {
		var params = req.params || {},
			body = req.body || {};

		req.log('patch milestone', params);

		Milestone.findOne(db, {
			id: +params.mid,
			ownerId: +params.userid
		}, function (err, model) {

			if (err) {
				req.log('error', err);
				res.json(err);
				return;
			}

			if (!model) {
				req.log('not found', params);
				res.notFound().send();
				return;
			}

			model = extend(model, body);
			model.ownerId = +model.ownerId;
			model.coreId = +model.coreId;

			model.save(db, function (err) {

				if (err && err instanceof Error) {
					req.fatal('fatal error', err);
					res.error().json(err);
					return;
				}

				if (err) {
					req.log('incorrect data from client', err);
					res.badRequest().json(err);
					return;
				}

				res.json({});
			});

		});

	}

	/**
	 * Метод удаляет milestone пользователя
	 * @param db
	 * @param req
	 * @param res
	 */
	function remove(db, req, res) {
		var params = req.params || {},
			deletedDate = Date.now();

		req.log('remove milestone', params);


		Milestone.findOne(db, {
			id: +params.mid,
			ownerId: +params.userid
		}, function (err, model) {

			if (err) {
				req.log('error', err);
				res.json(err);
				return;
			}

			model.deletedDate = deletedDate;

			async.series({

				ms: function (next) {
					model.save(db, next)
				},

				tasks: function (next) {
					req.log('remove tasks for milestone', model.id);
					Task.removeByMid(db, model.id, next)
				}

			}, function (err, info) {

				if (err) {
					req.log('error', err);
					res.json(err);
					return;
				}

				req.log('milestone removed');
				req.log('tasks removed', info.tasks);
				res.json({});
			});

		});



	}

	return {
		one: dbClient.open(getOne),
		patch: dbClient.open(patch),
		remove: dbClient.open(remove)
	};

};
