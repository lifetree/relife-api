var sessions = {};

var COLLECTION_NAME = 'sessions';

/**
 * Получить сессию пользователя
 * @param {string} secret - секрет пользователя логин:пароль
 * @param {DB} db - клиент базы
 * @param {function} done - функция обратного вызова
 */
exports.get = function (secret, db, done) {

	db.collection(COLLECTION_NAME).findOne({
		token: secret
	}, { _id: false }, done);

};

/**
 * Открыть новую сессию (если для этого пользователя уже есть ссесия, страя закрывается)
 * @param secret - секрет
 * @param user - информация о пользователе
 * @param {DB} db - клиент базы
 * @param {function} done - функция обратного вызова
 */
exports.open = function (secret, user, db, done) {
	var session = {
		token: secret,
		id: user.id
	};

	db.collection(COLLECTION_NAME).update({ token: secret }, session, {upsert: true}, function (err) {
		done(err, session);
	});

};
