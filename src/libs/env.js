exports.isProd = function () {
	return !!process.env.OPENSHIFT_NODEJS_PORT;
};

/**
 * Метод отдает нужный конфиг в зависимости от окружения
 */
exports.getConfig = function () {
	var config;

	config = exports.isProd()
		? require('../configs/app.prod')
		: require('../configs/app.dev');

	return config;
};


exports.getDbConfig = function () {
	var cfg = require('../configs/db');

	return exports.isProd()
		? cfg.prod
		: cfg.dev;

};
