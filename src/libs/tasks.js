var async = require('async');

var MS_IN_DAY = 86400000,
	MS_IN_WEEK = 604800000;

var DEFAULT_COUNT = 21;

var WEEKDAY_IDS = {
	"sun": 0,
	"mon": 1,
	"tue": 2,
	"wed": 3,
	"thu": 4,
	"fri": 5,
	"sat": 6
};

Date.prototype.setDay = function(dayOfWeek) {
	this.setDate(this.getDate() - this.getDay() + dayOfWeek);
};

var strategies = {

	/**
	 * Ставим задачу на следующий день
	 * @param prev - дата предыдущей задачи
	 * @returns {*}
	 */
	daily: function (prev) {
		return prev + MS_IN_DAY;
	},

	/**
	 * Ставим задачу на следующую неделю
	 * @param prev - дата предыдущей задачи
	 * @returns {*}
	 */
	weekly: function (prev) {
		return prev + MS_IN_WEEK;
	},

	/**
	 * Ставим задачу на следующий год
	 * @param prev - дата предыдущей задачи
	 * @returns {*}
	 */
	yearly: function (prev) {
		var dt = new Date(prev);
		dt.setYear(dt.getYear() + 1901);
		return dt.getTime();
	},

	/**
	 * Ставим задачу на дни неделиы
	 * @param prev - дата предыдущей задачи
	 * @param {object} duration - повторяемость задачи
	 * @returns {*}
	 */
	weekdays: function (prev, duration) {

		return duration.weekdays.map(function (weekday) {
			var date = new Date(prev),
				day = date.getDate();

			weekday = WEEKDAY_IDS[weekday];

			if (weekday > day) {
				date.setDay(weekday);
			} else {
				// если на этой неделе день недели прошел, ставим на следующую
				date.setDate(weekday + 6);
			}

			prev = date.getTime();
			return prev;
		});

	},

	/**
	 * Ставим задачу на интервал
	 * @param prev - дата предыдущей задачи
	 * @param {object} duration - повторяемость задачи
	 * @returns {*}
	 */
	interval: function (prev, duration) {
		return prev + duration.interval;
	}

};


/**
 *  Метод генерирует группу действи для заданного таска
 *  @params {Milestone} ms - мейлстоун к которому генерятся задачи
 *  @params {object} db - ссылка на базу данных
 *  @params {object} logger - логирование
 *  @params {function} done - колбек
 */
exports.generate = function (ms, db, logger, done) {
	var duration =  ms.thing.duration,
		count = duration.count || DEFAULT_COUNT,
		start = ms.thing.duration.startDate,
		collection = db.collection('tasks'),
		tasks = [];

	duration.penalty = duration.penalty || 0;
	logger.log('generate tasks for duration', duration, start);

	async.series({

		count: function (next) {
			collection.find({
				milestoneId: +ms.id
			}).count(next);
		},

		id: function (next) {
			collection.count(next);
		},

		removed: function (next) {

			collection.remove({
				milestoneId: +ms.id,
				planedDate: {$gt: start}
			}, next);

		}


	}, function (err, data) {
		var removed = data.removed.result.n,
			id;

		if (err) {
			return done(err);
		}

		if (!strategies[duration.strategy]) {
			return done({ thing: { duration: { strategy: 'invalid' } } });
		}

		id = data.id + 1;

		count = (count + duration.penalty) - (data.count - removed);
		logger.log('(count + duration.penalty) - (data.count - removed + 1)', count, data.count - removed );

		while (count > 0) {
			start = strategies[duration.strategy](start, duration);

			if (Array.isArray(start)) {
				tasks = tasks.concat(start);
				count = count - start.length;
			} else {
				tasks.push(start);
				count--;
			}

		}

		// удаляем лишний хвост
		if (count < 0) {

			while (count !== 0) {
				tasks.pop();
				count ++;
			}

		}

		logger.log('old tasks', data.count);
		logger.log('add tasks', tasks.length);
		logger.log('penalty tasks', duration.penalty);
		logger.log('remove tasks', removed);

		tasks = tasks.map(function (ts) {

			return {
				id: ++id,
				milestoneId: ms.id,
				coreId: ms.coreId,
				ownerId: ms.ownerId,
				planedDate: ts,
				createdDate: Date.now()
			};

		});

		logger.log('insert tasks for', {id: ms.id});
		collection.insert(tasks, done);
	});

};

/**
 * Метод удаляет таски мейлстоуна
 */
exports.remove = function (db, mid, userid, done) {

	db.collection('tasks').update({
		ownerId: +userid,
		milestoneId: +mid
	}, {
		$set: {
			deletedDate: Date.now()
		}
	}, {multi: true} , done);

};

/**
 * Метод начисляет штрафные таски за проеб
 */
exports.penalty = function (db, mid, userid, current, logger, done) {

	async.parallel({

		ms: function (next) {
			db.collection('milestones').findOne({userid: userid, id: mid}, next);
		},

		tasks: function (next) {

			// все таски, страше текущего
			db.collection('tasks').find({
				userid: userid,
				milestoneId: mid,
				planedDate: {$lte: current.planedDate}
			}).toArray(next);

		}

	}, function (err, data) {
		var penalty,
			n,
			i;

		if (err) {
			return done(err);
		}

		data.tasks = data.tasks.sort(function (a, b) {
			return a.planedDate - b.planedDate;
		});

		for (i = 0; i < data.tasks.length; i++) {

			if (!data.tasks[i].failed) {
				break;
			}

		}

		n = i + 1;
		penalty = (1 + n)/2 * n; // арифметическая прогрессия
		data.ms.thing.duration.penalty = penalty;
		logger.log('user have penalty', penalty);
		exports.generate(data.ms, db, logger, done)
	});

};
