var api = require('../../api/api'),
	url = require('url');

function createVar(variables, name) {
	var str = name;

	if (variables.indexOf(name) !== -1) {
		str = '{' + str + '=' + str + '}';
	} else {
		str = '{' + str + '}';
	}

	return str;
}

/**
 * "reports": {
            "POST*": "",
            "GET*": "",
            ":search": "GET",
            "{id}": {
                "GET*#/id": "",
                "PUT*#/id": "",
                "DELETE*#/id": "",
                "recipients": {
                    "GET*#/id": "",
                    ":search": "GET"
                }
            }
        }
 * @param paths
 * @returns {{}}
 */
function addSection(obj, name, spec, tail, root, all, variables) {
	var param = '';

	if (/\{.*\}/.test(name)) {
		param = name.replace(/\{(.*)\}/, '$1');
	}

	if (param && tail.indexOf(param) === -1) {
		tail.push(param);
	}

	if (obj[name]) {
		return tail;
	}

	obj[name] = {};

	Object.keys(spec).forEach(function (method) {
		var key = method.toUpperCase() + '*',
			localTail =  tail.filter(function (a) { return a !== param }),
			pathname = root + '/' + name,
			variable;

		if (localTail.length > 0) {
			key += '#/' + localTail.map(function (a) { return createVar(variables, a) }).join('#/');
		}

		if (param) {
			key += '#/' + createVar(variables, param) ;
		}

		obj[name][key] = '';

		if (spec[method].variables) {

			for (variable in spec[method].variables) {
				obj[name][key] += variable + '=' + spec[method].variables[variable];
			}

		}

		if (all[pathname] &&  all[pathname][method] && all[pathname][method]['__alias']) {
			obj[name][':' + all[pathname][method]['__alias']] = method.toUpperCase();
		}

	});

	return tail;
}

/**
 * Модуль собирает спецификацию для swager.io
 * @param {Object} spec - swagger спецификация ресурсов проекта
 * @return {Object} спецификация понятная apic.js
 */
exports.generate = function (spec, variables) {
	var result = {},
		paths = Object.keys(spec).sort();

	variables = variables || [];

	paths.forEach(function (name) {
		var cursor = result,
			params = [],
			root = '';

		name.replace(/^\//,'').split('/').forEach(function (segment){
			params = addSection(cursor, segment, spec[name], params, root, spec, variables);
			cursor = cursor[segment];
			root += '/' + segment;
		});

	});

	return result;
};
