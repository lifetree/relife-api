/**
 * Метод формирует представление об ресурсе по его url
 * @param {SwaggerSpec} spec - спецификация свагера
 * @param {string} uri - путь для которого нужно найти описание в свагер спецификации
 */

exports.normalizeUrl = function (url) {
	return url.replace(/(^\/|\/$)/ig, '');
};

/**
 * Метод формирует meta объект с информацией о контроллере и ресурсе
 * @param spec
 * @param url
 * @param method
 * @returns {{}}
 */
exports.getMetaByUri = function (spec, url, method) {
	var tests,
		tmp,
		info,
		res = {};

	url = exports.normalizeUrl(url);

	tests = Object.keys(spec.paths).sort(function (a, b) { return b.length - a.length }).map(function (uri) {
		var route = uri;

		uri = exports.normalizeUrl(uri);

		return {
			regex:  new RegExp(uri.replace(/\{[^\/]+\}/ig, '(.*)').replace(/\//ig, '\\/') + '$', 'ig'),
			route: route
		}

	});

	for (var i=0; i<tests.length; i++) {

		if (tests[i].regex.test(url)) {
			info = tests[i];
			break;
		}
	}

	if (!info) {
		return res;
	}

	res.route = info.route;

	if (!spec.paths[info.route][method]) {
		return res;
	}

	info = spec.paths[info.route][method];

	if (info.security) {

		res.security = info.security.map(function (type) {
			return Object.keys(type)[0];
		});

	}

	if (info.grants) {
		res.grants = info.grants;
	}

	if (!info.__controller) {
		return res;
	}

	tmp = info.__controller.split('.');

	res.controller = tmp[0];
	res.action = tmp[1];
	res.schema = info.__validateSchema;
	res.expressRoute = res.route.replace(/\{([^{^}]+)\}/ig, ':$1');

	return res;
};
