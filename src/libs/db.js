/***
 * Клиент для работой с бд
 */
var client = require('mongodb').MongoClient,
	env = require('./env'),
	config = env.getDbConfig();

var db;

exports.connect = function (done) {
	var uri = 'mongodb://';

	if (db) {
		return done(null, db);
	}

	if (config.username) {
		uri += config.username + ':' + config.password + '@';
	}

	if (config.uri) {
		uri = config.uri;
	} else {
		uri += config.host;
		uri += ':' + (config.port || 27017) + '/';
	}

	uri += config.db;
	console.log('DB: connect to', uri);

	client.connect(uri, function(err, res) {

		if (err) {
			return done(err);
		}

		db = res;
		done(null, db);
	});

};

/**
 * оборачивает метод и при его вызове внчале открывает доступ к бд
 * @param {Function} fn
 */
exports.open = function (fn) {

	return function (req, res, next) {

		exports.connect(function (err, db) {

			if (err) {
				res.error().json(err);
				return;
			}

			return fn(db, req, res, next);
		});

	}


};


