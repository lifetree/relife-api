var JaySchema = require('jayschema'),
	validator = new JaySchema();


function transform(errors) {
	var ret = {};

	function processError(error) {
		var fields = [];

		if (error.constraintName === 'required') {
			fields = error.desc.replace('missing: ', '').split(',') || [];
		}

		if (error.constraintName === 'format') {
			fields = [ error.constraintValue ];
			error.constraintName = 'invalid';
		}

		fields.forEach(function (name) {
			ret[name] = error.constraintName;
		});

	}

	errors.forEach(processError);
	return ret;
}

exports.validate = function (data, schema) {
	var errors = validator.validate(data, schema);

	if (errors && errors.length) {
		return transform(errors);
	}

	return null;
};
