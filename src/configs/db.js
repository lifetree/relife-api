var user = process.env.OPENSHIFT_MONGODB_DB_USERNAME || 'admin',
	pwd = process.env.OPENSHIFT_MONGODB_DB_PASSWORD,
	host = process.env.OPENSHIFT_MONGODB_DB_HOST;


module.exports = {
	"dev": {
		"host": "localhost",
		"db": "lifetree",
		"port": 27017
	},
	 "prod": {
		 "uri": process.env.OPENSHIFT_MONGODB_DB_URL,
		 "username": user,
		 "password": pwd,
		 "host":host,
		 "db": "relifeapi",
		 "port": process.env.OPENSHIFT_MONGODB_DB_PORT
	 }
};
