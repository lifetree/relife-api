var fs = require('fs'),
	path = require('path');

var cert = fs.readFileSync(path.resolve(__dirname, '../sslcert/relifeapi-imagemap.rhcloud.com.cert')),
	key = fs.readFileSync(path.resolve(__dirname, '../sslcert/relifeapi-imagemap.rhcloud.com.key'));


module.exports = {

	"api": {
		"basePath": "/api"
	},

	"cors": {
		"allowedOrigins": [
			"editor.swagger.io",
			"relife-imagemap.rhcloud.com",
			"localhost",
			"localhost:3000"
		],
		"headers": [
			"Authorization",
			"WWW-Authenticate",
			"X-Security-Token",
			"Content-Type"
		],

		"exposeHeaders": [
			"X-Security-Token"
		]

	},

	"app": {
		port: process.env.OPENSHIFT_NODEJS_PORT,
		ip: process.env.OPENSHIFT_NODEJS_IP,
		host: process.env.OPENSHIFT_APP_DNS
	}

};
