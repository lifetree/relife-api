var fs = require('fs'),
	path = require('path');

var cert = fs.readFileSync(path.resolve(__dirname, '../sslcert/localhost.cert')),
	key = fs.readFileSync(path.resolve(__dirname, '../sslcert/localhost.key'));

module.exports = {

	"api": {
		"basePath": "/api"
	},

	"app": {
		"port": 8080,
		"id": "127.0.0.1",
		//"ip": "100.96.165.33",
		"host": "localhost"
	},

	"ssl": {
		"cert": cert,
		"key": key
	},

	"cors": {
		"allowedOrigins": [
			"localhost",
			"http://editor.swagger.io",
			"localhost:3000"
		],

		"maxAge": 60000,

		"headers": [
			"Authorization",
			"WWW-Authenticate",
			"Content-Type",
			"X-Security-Token",
			"Accept"
		],

		"exposeHeaders": [
			"X-Security-Token"
		]

	}

};
