var jam = {
    "packages": [
        {
            "name": "async",
            "location": "packages/async",
            "main": "lib/async.js"
        },
        {
            "name": "jquery",
            "location": "packages/jquery",
            "main": "dist/jquery.js"
        }
    ],
    "version": "0.2.17",
    "shim": {}
};

if (typeof require !== "undefined" && require.config) {
    require.config({
    "packages": [
        {
            "name": "async",
            "location": "packages/async",
            "main": "lib/async.js"
        },
        {
            "name": "jquery",
            "location": "packages/jquery",
            "main": "dist/jquery.js"
        }
    ],
    "shim": {}
});
}
else {
    var require = {
    "packages": [
        {
            "name": "async",
            "location": "packages/async",
            "main": "lib/async.js"
        },
        {
            "name": "jquery",
            "location": "packages/jquery",
            "main": "dist/jquery.js"
        }
    ],
    "shim": {}
};
}

if (typeof exports !== "undefined" && typeof module !== "undefined") {
    module.exports = jam;
}