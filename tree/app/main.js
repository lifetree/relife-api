define(function (require) {
	var d3 = require('d3'),
		$ = require('jquery'),
		api = require('api');

	var Dream = require('shapes/dream');

	var $win = $(window),
		svg;

	function draw() {
		svg = d3.select('svg')
			.attr('width', $win.width())
			.attr('height', $win.height());

		svg.append("rect")
			.attr("id", 'sky')
			.attr("width", $win.width())
			.attr("height", 200)
			.style("fill", "url(#skyGradient)");

		var dream = new Dream();

		dream.render(svg);
	}

	api.authorize.basic('test@re.life', 'test');

	api.users.cores.list({userid: 1}, function (err, res) {
		console.log(res);
		draw();
	});


});
