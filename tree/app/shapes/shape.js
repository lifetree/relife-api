define(function (require) {
	var d3 = require('d3');

	var Shape = function () {
	};

	Shape.prototype.render =  function (svg) {

		if (this.node) {
			return;
		}

		this.node = svg.selectAll('g')
			.data(this.dataset)
			.enter()
			.append('path')
			.attr('d', this.shapes[0])
			.attr('fill', this.options.background)
			.text(function (d) { return d; })
			.on('click', this.onClick.bind(this))
			.append('text')
			.text('hellowolrd');

	};

	Shape.prototype.onClick = function () {
		console.log(this.type, 1234);
	};

	return Shape;
});
