define(function (require) {
	var apic = require('../../client/apic'),
		desc = require('../../client/descriptor');

	return apic(desc);
});
